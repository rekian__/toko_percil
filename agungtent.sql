-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 26, 2019 at 01:06 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agungtent`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reply_to` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(33, '2019_12_06_150504_create_project_category', 2),
(34, '2019_12_06_151738_create_project', 2),
(35, '2019_12_06_151805_create_product_category', 2),
(36, '2019_12_06_151813_create_product', 2),
(37, '2019_12_06_151908_create_slider', 2),
(38, '2019_12_06_151918_create_contact_us', 2),
(39, '2019_12_06_154818_create_posts', 2),
(40, '2019_12_06_154825_create_tags', 2),
(41, '2019_12_15_000725_alter_products', 3),
(42, '2019_12_23_154940_alter_slider_add_column', 4),
(44, '2019_12_24_015134_create_page', 5);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_full` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_type_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `description`, `content`, `status`, `image`, `image_full`, `image_thumbnail`, `caption`, `page_type_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, NULL, 1, NULL, '/images/1577180794AGUNG-TENT-LOGO-1.png', '/images/1577180794AGUNG-TENT-LOGO-1.png', NULL, 1, NULL, NULL, NULL, NULL),
(2, NULL, '1577243540', NULL, '<p class=\"small text-muted mb-4\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam <a href=\"#\">readmore</a></p>', 1, NULL, NULL, NULL, NULL, 4, 1, NULL, '2019-12-24 20:12:20', '2019-12-24 20:14:05'),
(3, '111 _ Bagaimanahakn Cara Memesan Tenda di Agung Tent?', '1577266569', NULL, '<p>Bagaimanahakn Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban&nbsp;</p>', 1, NULL, NULL, NULL, NULL, 5, 1, NULL, '2019-12-25 02:36:09', '2019-12-25 02:36:09'),
(4, '222 __ Bagaimanahakn Cara Memesan Tenda di Agung Tent?', '1577266562', NULL, '<p>Bagaimanahakn Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban&nbsp;</p>', 1, NULL, NULL, NULL, NULL, 5, 1, NULL, '2019-12-25 02:36:09', '2019-12-25 02:36:09'),
(5, '333 __ Bagaimanahakn Cara Memesan Tenda di Agung Tent?', '1577266563', NULL, '<p>Bagaimanahakn Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban&nbsp;</p>', 1, NULL, NULL, NULL, NULL, 5, 1, NULL, '2019-12-25 02:36:09', '2019-12-25 02:36:09'),
(6, '444 __ Bagaimanahakn Cara Memesan Tenda di Agung Tent?', '1577266564', NULL, '<p>Bagaimanahakn Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban Cara Memesan Tenda di Agung Tent? Jawaban&nbsp;</p>', 1, NULL, NULL, NULL, NULL, 5, 1, NULL, '2019-12-25 02:36:09', '2019-12-25 02:36:09'),
(7, 'Our Client', '1577267753-our-client', NULL, NULL, 1, NULL, NULL, NULL, NULL, 6, 1, NULL, '2019-12-25 02:55:53', '2019-12-25 02:55:53'),
(8, 'Tenda Roder', '1577269891-tenda-roder', 'Tenda Roder Ukuran 10m, 15m, 20m', NULL, 1, 'help.jpg', '/images/1577270315help.jpg', '/images/thumbnails/1577270315help.jpg', NULL, 7, 1, NULL, '2019-12-25 03:31:31', '2019-12-25 03:38:35'),
(9, 'Tenda Dome Geodesic', '1577269891-tenda-roder', 'Tenda Dome Geodesic ukuran 6m, 10m, 15m, dan 20m', NULL, 1, 'about.jpg', '/images/1577270201about.jpg', '/images/thumbnails/1577270201about.jpg', NULL, 7, 1, NULL, '2019-12-25 03:31:31', '2019-12-25 03:39:24'),
(10, 'Flooring', '1577269891-tenda-roder', 'Flooring / Lantai Tenda dengan luas menyesuaikan ukuran tenda', NULL, 1, 'periksa.jpg', '/images/1577270407periksa.jpg', '/images/thumbnails/1577270407periksa.jpg', NULL, 7, 1, NULL, '2019-12-25 03:31:31', '2019-12-25 03:40:07'),
(11, 'Persewaan', '1577270660-persewaan', 'Sewa Tenda Roder, Tenda Dome, dan Flooring', NULL, 1, 'tipstrik.jpg', '/images/1577270660tipstrik.jpg', '/images/thumbnails/1577270660tipstrik.jpg', NULL, 8, 1, NULL, '2019-12-25 03:44:20', '2019-12-25 03:44:20'),
(12, 'Penjualan', '1577270688-penjualan', 'Menjual Tenda Roder Baru dan Tenda Roder Second', NULL, 1, 'about.jpg', '/images/1577270688about.jpg', '/images/thumbnails/1577270688about.jpg', NULL, 8, 1, NULL, '2019-12-25 03:44:48', '2019-12-25 03:44:48');

-- --------------------------------------------------------

--
-- Table structure for table `page_images`
--

CREATE TABLE `page_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_full` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_images`
--

INSERT INTO `page_images` (`id`, `image`, `image_full`, `image_thumbnail`, `caption`, `page_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'about.jpg', '/images/1577267754about.jpg', '/images/thumbnails/1577267754about.jpg', 'free caption image', 7, 1, NULL, '2019-12-25 02:55:54', '2019-12-25 02:55:54'),
(2, 'help.jpg', '/images/1577267755help.jpg', '/images/thumbnails/1577267755help.jpg', 'free caption image', 7, 1, NULL, '2019-12-25 02:55:55', '2019-12-25 02:55:55'),
(3, 'periksa.jpg', '/images/1577267755periksa.jpg', '/images/thumbnails/1577267755periksa.jpg', 'free caption image', 7, 1, NULL, '2019-12-25 02:55:55', '2019-12-25 02:55:55'),
(4, 'solusi.jpg', '/images/1577267756solusi.jpg', '/images/thumbnails/1577267756solusi.jpg', 'free caption image', 7, 1, NULL, '2019-12-25 02:55:56', '2019-12-25 02:55:56'),
(5, 'tipstrik.jpg', '/images/1577267756tipstrik.jpg', '/images/thumbnails/1577267756tipstrik.jpg', 'free caption image', 7, 1, NULL, '2019-12-25 02:55:56', '2019-12-25 02:55:56');

-- --------------------------------------------------------

--
-- Table structure for table `page_types`
--

CREATE TABLE `page_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_types`
--

INSERT INTO `page_types` (`id`, `title`, `slug`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Icon Header', 'icon-header', 1, 1, NULL, '2019-12-24 01:11:50', '2019-12-24 01:11:50'),
(2, 'Icon Home', 'icon-home', 1, 1, NULL, '2019-12-24 03:17:29', '2019-12-24 03:17:29'),
(3, 'Home Info', 'home-info', 1, 1, NULL, '2019-12-24 03:18:18', '2019-12-24 03:18:18'),
(4, 'Home About Us', 'home-about-us', 1, 1, NULL, '2019-12-24 03:18:59', '2019-12-24 03:18:59'),
(5, 'Home FAQ', 'home-faq', 1, 1, NULL, '2019-12-24 03:19:23', '2019-12-24 03:19:23'),
(6, 'Home Our Client', 'home-our-client', 1, 1, NULL, '2019-12-24 03:19:45', '2019-12-24 03:24:01'),
(7, 'Icon Produk Kami', 'icon-produk-kami', 1, 1, NULL, '2019-12-25 03:30:41', '2019-12-25 03:30:41'),
(8, 'icon-layanan-kami', 'icon-layanan-kami', 1, 1, NULL, '2019-12-25 03:42:59', '2019-12-25 03:42:59');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_full` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `description`, `content`, `image`, `image_full`, `image_thumbnail`, `caption`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Post 1', '1576108630-post-1', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1', '<p>Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1&nbsp;</p>\r\n<p><img src=\"../../../images/157711952120160526-mothercare.jpg\" alt=\"\" width=\"690\" height=\"350\" /></p>\r\n<p>Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1 Post 1&nbsp;</p>', '6181389217_f16bebdbdb_z.jpg', '/images/15771203026181389217_f16bebdbdb_z.jpg', '/images/thumbnails/15771203026181389217_f16bebdbdb_z.jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-23 09:58:22'),
(3, 'Post 2', '1576108630-post-2', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(4, 'Post 3', '1576108630-post-3', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(5, 'Post 4', '1576108630-post-4', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(6, 'Post 5', '1576108630-post-5', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(7, 'Post 6', '1576108630-post-6', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(8, 'Post 7', '1576108630-post-7', 'sdPost 1\r\nsdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(9, 'Post 8', '1576108630-post-8', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(10, 'Post 1', '1576108630-post-11', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(11, 'Post 2', '1576108630-post-21', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(12, 'Post 3', '1576108630-post-31', 'sdPost 1\r\nsdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(13, 'Post 4', '1576108630-post-41', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(14, 'Post 5', '1576108630-post-51', 'sdPost 1\r\nsdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(15, 'Post 6', '1576108630-post-61', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(16, 'Post 7', '1576108630-post-71', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38'),
(17, 'Post 8', '1576108630-post-81', 'sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\nPost 1sdPost 1\r\n', '<p>Post 1</p>', '3701912368_f807268f49_b (1).jpg', '/images/15761086303701912368_f807268f49_b (1).jpg', '/images/thumbnails/15761086303701912368_f807268f49_b (1).jpg', 'Post 1', 1, 1, NULL, '2019-12-11 16:57:10', '2019-12-11 16:57:38');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `product_category_id` int(10) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `spesification` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `slug`, `description`, `content`, `status`, `product_category_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `spesification`) VALUES
(3, 'Tenda Dome 10x10', '1575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 3, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46', '<p>Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification&nbsp;</p>'),
(4, 'Tenda Dome 10x10', '11575811234-tenda-dome-10x101', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 3, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46', '<p>Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification&nbsp;</p>'),
(5, 'Tenda Dome 10x10', '11575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 3, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46', '<p>Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification&nbsp;</p>'),
(6, 'Tenda Dome 10x10', '111575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 3, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46', '<p>Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification&nbsp;</p>'),
(7, 'Tenda Dome 10x10', '31575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 3, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46', '<p>Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification&nbsp;</p>'),
(8, 'Tenda Dome 10x10', '311575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 3, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46', '<p>Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification&nbsp;</p>'),
(9, 'Tenda Dome 10x10', '311575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 3, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46', '<p>Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification&nbsp;</p>'),
(10, 'Tenda Dome 10x10', '3111575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 10, 1, NULL, '2019-12-08 06:20:35', '2019-12-22 00:39:33', '<p>Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification Tenda Dome 10x10 spesification&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_full` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `title`, `slug`, `description`, `content`, `image`, `image_full`, `image_thumbnail`, `caption`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Projection Dome', '1575796323-tenda-dome', 'Projection Dome', '<p>Projection DomeProjection DomeProjection DomeProjection DomeProjection Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome&nbsp;</p>', 'stock-exchange-913981_1920.png', '/images/1576189083stock-exchange-913981_1920.png', '/images/thumbnails/1576189083stock-exchange-913981_1920.png', 'Projection Dome', 1, 1, NULL, '2019-12-08 02:12:03', '2019-12-12 15:18:13'),
(2, 'Fabric Dome', '1576189140-fabric-dome', 'Fabric Dome Fabric Dome Fabric Dome Fabric Dome Fabric Dome', '<p>Fabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric Dome</p>', 'unionpay.jpg', '/images/1576189140unionpay.jpg', '/images/thumbnails/1576189140unionpay.jpg', 'Fabric DomeFabric DomeFabric Dome', 1, 1, NULL, '2019-12-12 15:19:00', '2019-12-12 15:19:00'),
(3, 'Glass Dome', '1576189168-glass-dome', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', '<p>Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome</p>', '15456306192_feec27349a_z.jpg', '/images/157618916815456306192_feec27349a_z.jpg', '/images/thumbnails/157618916815456306192_feec27349a_z.jpg', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', 1, 1, NULL, '2019-12-12 15:19:28', '2019-12-12 15:19:28'),
(4, 'Projection Dome', '11575796323-tenda-dome', 'Projection Dome', '<p>Projection DomeProjection DomeProjection DomeProjection DomeProjection Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome&nbsp;</p>', 'stock-exchange-913981_1920.png', '/images/1576189083stock-exchange-913981_1920.png', '/images/thumbnails/1576189083stock-exchange-913981_1920.png', 'Projection Dome', 1, 1, NULL, '2019-12-08 02:12:03', '2019-12-12 15:18:13'),
(5, 'Fabric Dome', '11576189140-fabric-dome', 'Fabric Dome Fabric Dome Fabric Dome Fabric Dome Fabric Dome', '<p>Fabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric Dome</p>', 'unionpay.jpg', '/images/1576189140unionpay.jpg', '/images/thumbnails/1576189140unionpay.jpg', 'Fabric DomeFabric DomeFabric Dome', 1, 1, NULL, '2019-12-12 15:19:00', '2019-12-12 15:19:00'),
(6, 'Glass Dome', '11576189168-glass-dome', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', '<p>Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome</p>', '15456306192_feec27349a_z.jpg', '/images/157618916815456306192_feec27349a_z.jpg', '/images/thumbnails/157618916815456306192_feec27349a_z.jpg', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', 1, 1, NULL, '2019-12-12 15:19:28', '2019-12-12 15:19:28'),
(7, 'Projection Dome', '21575796323-tenda-dome', 'Projection Dome', '<p>Projection DomeProjection DomeProjection DomeProjection DomeProjection Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome&nbsp;</p>', 'stock-exchange-913981_1920.png', '/images/1576189083stock-exchange-913981_1920.png', '/images/thumbnails/1576189083stock-exchange-913981_1920.png', 'Projection Dome', 1, 1, NULL, '2019-12-08 02:12:03', '2019-12-12 15:18:13'),
(8, 'Fabric Dome', '21576189140-fabric-dome', 'Fabric Dome Fabric Dome Fabric Dome Fabric Dome Fabric Dome', '<p>Fabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric Dome</p>', 'unionpay.jpg', '/images/1576189140unionpay.jpg', '/images/thumbnails/1576189140unionpay.jpg', 'Fabric DomeFabric DomeFabric Dome', 1, 1, NULL, '2019-12-12 15:19:00', '2019-12-12 15:19:00'),
(9, 'Glass Dome', '21576189168-glass-dome', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', '<p>Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome</p>', '15456306192_feec27349a_z.jpg', '/images/157618916815456306192_feec27349a_z.jpg', '/images/thumbnails/157618916815456306192_feec27349a_z.jpg', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', 1, 1, NULL, '2019-12-12 15:19:28', '2019-12-12 15:19:28'),
(10, 'Projection Dome', '211575796323-tenda-dome', 'Projection Dome', '<p>Projection DomeProjection DomeProjection DomeProjection DomeProjection Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome&nbsp;</p>', 'stock-exchange-913981_1920.png', '/images/1576189083stock-exchange-913981_1920.png', '/images/thumbnails/1576189083stock-exchange-913981_1920.png', 'Projection Dome', 1, 1, NULL, '2019-12-08 02:12:03', '2019-12-12 15:18:13'),
(11, 'Fabric Dome', '211576189140-fabric-dome', 'Fabric Dome Fabric Dome Fabric Dome Fabric Dome Fabric Dome', '<p>Fabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric Dome</p>', 'unionpay.jpg', '/images/1576189140unionpay.jpg', '/images/thumbnails/1576189140unionpay.jpg', 'Fabric DomeFabric DomeFabric Dome', 1, 1, NULL, '2019-12-12 15:19:00', '2019-12-12 15:19:00'),
(12, 'Glass Dome', '211576189168-glass-dome', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', '<p>Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome</p>', '15456306192_feec27349a_z.jpg', '/images/157618916815456306192_feec27349a_z.jpg', '/images/thumbnails/157618916815456306192_feec27349a_z.jpg', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', 1, 1, NULL, '2019-12-12 15:19:28', '2019-12-12 15:19:28'),
(13, 'Projection Dome', '31575796323-tenda-dome', 'Projection Dome', '<p>Projection DomeProjection DomeProjection DomeProjection DomeProjection Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome&nbsp;</p>', 'stock-exchange-913981_1920.png', '/images/1576189083stock-exchange-913981_1920.png', '/images/thumbnails/1576189083stock-exchange-913981_1920.png', 'Projection Dome', 1, 1, NULL, '2019-12-08 02:12:03', '2019-12-12 15:18:13'),
(14, 'Fabric Dome', '31576189140-fabric-dome', 'Fabric Dome Fabric Dome Fabric Dome Fabric Dome Fabric Dome', '<p>Fabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric Dome</p>', 'unionpay.jpg', '/images/1576189140unionpay.jpg', '/images/thumbnails/1576189140unionpay.jpg', 'Fabric DomeFabric DomeFabric Dome', 1, 1, NULL, '2019-12-12 15:19:00', '2019-12-12 15:19:00'),
(15, 'Glass Dome', '31576189168-glass-dome', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', '<p>Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome</p>', '15456306192_feec27349a_z.jpg', '/images/157618916815456306192_feec27349a_z.jpg', '/images/thumbnails/157618916815456306192_feec27349a_z.jpg', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', 1, 1, NULL, '2019-12-12 15:19:28', '2019-12-12 15:19:28'),
(16, 'Projection Dome', '311575796323-tenda-dome', 'Projection Dome', '<p>Projection DomeProjection DomeProjection DomeProjection DomeProjection Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome&nbsp;</p>', 'stock-exchange-913981_1920.png', '/images/1576189083stock-exchange-913981_1920.png', '/images/thumbnails/1576189083stock-exchange-913981_1920.png', 'Projection Dome', 1, 1, NULL, '2019-12-08 02:12:03', '2019-12-12 15:18:13'),
(17, 'Fabric Dome', '311576189140-fabric-dome', 'Fabric Dome Fabric Dome Fabric Dome Fabric Dome Fabric Dome', '<p>Fabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric Dome</p>', 'unionpay.jpg', '/images/1576189140unionpay.jpg', '/images/thumbnails/1576189140unionpay.jpg', 'Fabric DomeFabric DomeFabric Dome', 1, 1, NULL, '2019-12-12 15:19:00', '2019-12-12 15:19:00'),
(18, 'Glass Dome', '311576189168-glass-dome', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', '<p>Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome</p>', '15456306192_feec27349a_z.jpg', '/images/157618916815456306192_feec27349a_z.jpg', '/images/thumbnails/157618916815456306192_feec27349a_z.jpg', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', 1, 1, NULL, '2019-12-12 15:19:28', '2019-12-12 15:19:28'),
(19, 'Projection Dome', '321575796323-tenda-dome', 'Projection Dome', '<p>Projection DomeProjection DomeProjection DomeProjection DomeProjection Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome&nbsp;</p>', 'stock-exchange-913981_1920.png', '/images/1576189083stock-exchange-913981_1920.png', '/images/thumbnails/1576189083stock-exchange-913981_1920.png', 'Projection Dome', 1, 1, NULL, '2019-12-08 02:12:03', '2019-12-12 15:18:13'),
(20, 'Fabric Dome', '321576189140-fabric-dome', 'Fabric Dome Fabric Dome Fabric Dome Fabric Dome Fabric Dome', '<p>Fabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric Dome</p>', 'unionpay.jpg', '/images/1576189140unionpay.jpg', '/images/thumbnails/1576189140unionpay.jpg', 'Fabric DomeFabric DomeFabric Dome', 1, 1, NULL, '2019-12-12 15:19:00', '2019-12-12 15:19:00'),
(21, 'Glass Dome', '321576189168-glass-dome', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', '<p>Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome</p>', '15456306192_feec27349a_z.jpg', '/images/157618916815456306192_feec27349a_z.jpg', '/images/thumbnails/157618916815456306192_feec27349a_z.jpg', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', 1, 1, NULL, '2019-12-12 15:19:28', '2019-12-12 15:19:28'),
(22, 'Projection Dome', '3211575796323-tenda-dome', 'Projection Dome', '<p>Projection DomeProjection DomeProjection DomeProjection DomeProjection Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome</p>\r\n<p>Tenda Dome Tenda Dome&nbsp;</p>', 'stock-exchange-913981_1920.png', '/images/1576189083stock-exchange-913981_1920.png', '/images/thumbnails/1576189083stock-exchange-913981_1920.png', 'Projection Dome', 1, 1, NULL, '2019-12-08 02:12:03', '2019-12-12 15:18:13'),
(23, 'Fabric Dome', '3211576189140-fabric-dome', 'Fabric Dome Fabric Dome Fabric Dome Fabric Dome Fabric Dome', '<p>Fabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric DomeFabric Dome</p>', 'unionpay.jpg', '/images/1576189140unionpay.jpg', '/images/thumbnails/1576189140unionpay.jpg', 'Fabric DomeFabric DomeFabric Dome', 1, 1, NULL, '2019-12-12 15:19:00', '2019-12-12 15:19:00'),
(24, 'Glass Dome', '3211576189168-glass-dome', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', '<p>Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome</p>', '15456306192_feec27349a_z.jpg', '/images/157618916815456306192_feec27349a_z.jpg', '/images/thumbnails/157618916815456306192_feec27349a_z.jpg', 'Glass DomeGlass DomeGlass DomeGlass DomeGlass DomeGlass Dome', 1, 1, NULL, '2019-12-12 15:19:28', '2019-12-12 15:19:28');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_full` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `image`, `image_full`, `image_thumbnail`, `caption`, `product_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(5, '3701912368_f807268f49_b.jpg', '/images/15763740473701912368_f807268f49_b.jpg', '/images/thumbnails/15763740473701912368_f807268f49_b.jpg', 'free caption image', 3, 1, NULL, '2019-12-14 18:40:47', '2019-12-14 18:40:47'),
(6, '6181389217_f16bebdbdb_z.jpg', '/images/15763740476181389217_f16bebdbdb_z.jpg', '/images/thumbnails/15763740476181389217_f16bebdbdb_z.jpg', 'free caption image', 3, 1, NULL, '2019-12-14 18:40:47', '2019-12-14 18:40:47'),
(7, '6214794900_d2889e6fa7_b (1).jpg', '/images/15763740476214794900_d2889e6fa7_b (1).jpg', '/images/thumbnails/15763740476214794900_d2889e6fa7_b (1).jpg', 'free caption image', 3, 1, NULL, '2019-12-14 18:40:47', '2019-12-14 18:40:47'),
(8, 'credit-card-scissors.jpg', '/images/1576374048credit-card-scissors.jpg', '/images/thumbnails/1576374048credit-card-scissors.jpg', 'free caption image', 3, 1, NULL, '2019-12-14 18:40:48', '2019-12-14 18:40:48');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `project_category_id` int(10) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `title`, `slug`, `description`, `content`, `status`, `project_category_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'Tenda Dome 10x10', '1575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 3, 1, NULL, '2019-12-08 06:20:35', '2019-12-22 00:45:27'),
(4, 'Tenda Dome 10x10', '11575811234-tenda-dome-10x101', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 15, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46'),
(5, 'Tenda Dome 10x10', '11575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 15, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46'),
(6, 'Tenda Dome 10x10', '111575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 15, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46'),
(7, 'Tenda Dome 10x10', '31575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 15, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46'),
(8, 'Tenda Dome 10x10', '311575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 16, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46'),
(9, 'Tenda Dome 10x10', '311575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 2, 16, 1, NULL, '2019-12-08 06:20:35', '2019-12-21 23:59:16'),
(10, 'Tenda Dome 10x10', '3111575811234-tenda-dome-10x10', 'Tenda Dome 10x10 desc', '<p>Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content Tenda Dome 10x10 content&nbsp;</p>', 1, 16, 1, NULL, '2019-12-08 06:20:35', '2019-12-14 18:40:46');

-- --------------------------------------------------------

--
-- Table structure for table `project_categories`
--

CREATE TABLE `project_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_full` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_categories`
--

INSERT INTO `project_categories` (`id`, `title`, `slug`, `description`, `content`, `image`, `image_full`, `image_thumbnail`, `caption`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Ceremony', '1576847725-ceremony', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(2, 'Glamping Living', '1576847725-glamping-living', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(3, 'Projection Show', '1576847725-projection-show', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(4, 'Car Show', '1576847725-car-show', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(5, 'Ceremony', '1576847725-ceremony1', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(6, 'Glamping Living', '1576847725-glamping-living1', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(7, 'Projection Show', '1576847725-projection-show1', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(8, 'Car Show', '1576847725-car-show1', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(9, 'Ceremony', '1576847725-ceremony2', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(10, 'Glamping Living', '1576847725-glamping-living2', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(11, 'Projection Show', '1576847725-projection-show2', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(12, 'Car Show', '1576847725-car-show2', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(13, 'Ceremony', '1576847725-ceremony12', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(14, 'Glamping Living', '1576847725-glamping-living12', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(15, 'Projection Show', '1576847725-projection-show12', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25'),
(16, 'Car Show', '1576847725-car-show12', 'Ceremony Ceremony Ceremony Ceremony', '<p>Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony Ceremony&nbsp;</p>', '2280082169_8488bc7518_z.jpg', '/images/15768477252280082169_8488bc7518_z.jpg', '/images/thumbnails/15768477252280082169_8488bc7518_z.jpg', 'Ceremony Ceremony Ceremony Ceremony', 1, 1, NULL, '2019-12-20 06:15:25', '2019-12-20 06:15:25');

-- --------------------------------------------------------

--
-- Table structure for table `project_images`
--

CREATE TABLE `project_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_full` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_images`
--

INSERT INTO `project_images` (`id`, `image`, `image_full`, `image_thumbnail`, `caption`, `project_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, '6181389217_f16bebdbdb_z.jpg', '/images/15769979566181389217_f16bebdbdb_z.jpg', '/images/thumbnails/15769979566181389217_f16bebdbdb_z.jpg', 'free caption image', 9, 1, NULL, '2019-12-21 23:59:16', '2019-12-21 23:59:16'),
(2, '6214794900_d2889e6fa7_b (1).jpg', '/images/15769979576214794900_d2889e6fa7_b (1).jpg', '/images/thumbnails/15769979576214794900_d2889e6fa7_b (1).jpg', 'free caption image', 9, 1, NULL, '2019-12-21 23:59:17', '2019-12-21 23:59:17'),
(3, '12169665653_2760da0a48_z.jpg', '/images/157699795712169665653_2760da0a48_z.jpg', '/images/thumbnails/157699795712169665653_2760da0a48_z.jpg', 'free caption image', 9, 1, NULL, '2019-12-21 23:59:17', '2019-12-21 23:59:17'),
(4, 'barcacard.png', '/images/1577109642barcacard.png', '/images/thumbnails/1577109642barcacard.png', 'free caption image', 3, 1, NULL, '2019-12-23 07:00:42', '2019-12-23 07:00:42'),
(5, 'billets-de-banque-euros-1456165834Zs3.jpg', '/images/1577109643billets-de-banque-euros-1456165834Zs3.jpg', '/images/thumbnails/1577109643billets-de-banque-euros-1456165834Zs3.jpg', 'free caption image', 3, 1, NULL, '2019-12-23 07:00:43', '2019-12-23 07:00:43'),
(6, 'bni-hotel.jpg', '/images/1577109644bni-hotel.jpg', '/images/thumbnails/1577109644bni-hotel.jpg', 'free caption image', 3, 1, NULL, '2019-12-23 07:00:44', '2019-12-23 07:00:44'),
(7, 'buy and sell.jpg', '/images/1577109644buy and sell.jpg', '/images/thumbnails/1577109644buy and sell.jpg', 'free caption image', 3, 1, NULL, '2019-12-23 07:00:44', '2019-12-23 07:00:44');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `position` enum('left','center','right') COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_style` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `description`, `image`, `caption`, `url`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `position`, `text_style`) VALUES
(1, 'Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet', '/images/1576188186bank-indonesia2-130625-b.jpg', 'Slider the Best', 'slider.com', 1, 1, 1, '2019-12-08 03:06:11', '2019-12-12 15:04:31', 'left', 1),
(2, 'Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet', '/images/1577180794AGUNG-TENT-LOGO-1.png', 'Lorem ipsum dolor sit amet', 'google.com', 1, 1, 1, '2019-12-12 15:04:06', '2019-12-24 02:46:34', 'center', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tagged`
--

CREATE TABLE `tagged` (
  `tag_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'rekian', 'rekian.dewandaru@gmail.com', NULL, '$2y$10$Fs9NwYGO.xmWIymMPd3vnO2DNMyPqBQInhbmMEdnsraCocM6dH19q', NULL, '2019-11-22 11:22:36', '2019-11-22 11:22:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contact_us_created_by_foreign` (`created_by`),
  ADD KEY `contact_us_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_page_type_id_foreign` (`page_type_id`),
  ADD KEY `pages_created_by_foreign` (`created_by`),
  ADD KEY `pages_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `page_images`
--
ALTER TABLE `page_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_images_page_id_foreign` (`page_id`),
  ADD KEY `page_images_created_by_foreign` (`created_by`),
  ADD KEY `page_images_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `page_types`
--
ALTER TABLE `page_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_types_created_by_foreign` (`created_by`),
  ADD KEY `page_types_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_created_by_foreign` (`created_by`),
  ADD KEY `posts_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_product_category_id_foreign` (`product_category_id`),
  ADD KEY `products_created_by_foreign` (`created_by`),
  ADD KEY `products_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_categories_created_by_foreign` (`created_by`),
  ADD KEY `product_categories_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`),
  ADD KEY `product_images_created_by_foreign` (`created_by`),
  ADD KEY `product_images_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projects_project_category_id_foreign` (`project_category_id`),
  ADD KEY `projects_created_by_foreign` (`created_by`),
  ADD KEY `projects_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `project_categories`
--
ALTER TABLE `project_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_categories_created_by_foreign` (`created_by`),
  ADD KEY `project_categories_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `project_images`
--
ALTER TABLE `project_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_images_project_id_foreign` (`project_id`),
  ADD KEY `project_images_created_by_foreign` (`created_by`),
  ADD KEY `project_images_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sliders_created_by_foreign` (`created_by`),
  ADD KEY `sliders_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `tagged`
--
ALTER TABLE `tagged`
  ADD KEY `tagged_tag_id_foreign` (`tag_id`),
  ADD KEY `tagged_post_id_foreign` (`post_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tags_created_by_foreign` (`created_by`),
  ADD KEY `tags_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `page_images`
--
ALTER TABLE `page_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `page_types`
--
ALTER TABLE `page_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `project_categories`
--
ALTER TABLE `project_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `project_images`
--
ALTER TABLE `project_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD CONSTRAINT `contact_us_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contact_us_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `pages_page_type_id_foreign` FOREIGN KEY (`page_type_id`) REFERENCES `page_types` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `pages_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `page_images`
--
ALTER TABLE `page_images`
  ADD CONSTRAINT `page_images_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `page_images_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `page_images_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `page_types`
--
ALTER TABLE `page_types`
  ADD CONSTRAINT `page_types_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `page_types_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_product_category_id_foreign` FOREIGN KEY (`product_category_id`) REFERENCES `product_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD CONSTRAINT `product_categories_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_categories_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_images_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `projects_project_category_id_foreign` FOREIGN KEY (`project_category_id`) REFERENCES `project_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `projects_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `project_categories`
--
ALTER TABLE `project_categories`
  ADD CONSTRAINT `project_categories_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_categories_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `project_images`
--
ALTER TABLE `project_images`
  ADD CONSTRAINT `project_images_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_images_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_images_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `sliders_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sliders_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tagged`
--
ALTER TABLE `tagged`
  ADD CONSTRAINT `tagged_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tagged_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tags_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
