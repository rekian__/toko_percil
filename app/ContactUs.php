<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model{

	public function getContactUs(){
        return $this->orderBy('created_at', 'DESC')->paginate(1);
	}

    public function getDetailContactUs($id){
        return $this->where("id","=",$id)->first();
    }

    public function deleteData($target_route, $model){
        if ($model->delete()) {
            return redirect()->action($target_route);
        }
    }
}
