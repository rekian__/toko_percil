<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class Dashboard extends Model{

    public function saveImage($request){
        $input = $request->all();
        $savedName = "";
    	if ($request->file('file') != null) {
	        $originalImage = $request->file('file');
	        $fullImage = Image::make($originalImage);
	        $originalUrl = '/images/';
	        $originalPath = public_path().$originalUrl;
	        $fullImage->save($originalPath.time().$originalImage->getClientOriginalName());
	        $savedName = $originalUrl.time().$originalImage->getClientOriginalName();
	    }
            
        return json_encode(['location' => $savedName ]);
    }
}
