<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Page;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share( 'header_logo', (new Page)->getHeaderLogo()->first());
        View::share( 'homeAboutUs', (new Page)->getPageByPageTypeSlug("home-about-us")->first());
        View::share( 'contactFooter', (new Page)->getPageByPageTypeSlug("contact-footer")->first());
    }
}
