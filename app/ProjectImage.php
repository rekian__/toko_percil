<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use Illuminate\Support\Facades\Auth;

class ProjectImage extends Model{

	public function project() {
        return $this->belongsTo('App\Project');
    }

	protected $fillable = [
		"id",
		"image",
		"image_thumbnail",
		"image_full",
		"caption",
		"project_id",
		"created_by",
    ];

    public function deleteData($target_route, $model){
        if ($model->delete()) {
            return redirect()->action($target_route);
        }
    }

    public function saveModelImage($request, $model){
        $savedNameFull = "";
        $savedNameThumbnail = "";
        $savedName = "";
    	if ($request->file('images') != null) {
    		foreach ($request->file('images') as $key => $originalImage) {

		        $fullImage = Image::make($originalImage);
		        $thumbnailImage = Image::make($originalImage)->resize(315, 156);
		        
		        $originalUrl = '/images/';
		        $originalPath = public_path().$originalUrl;
		        $fullImage->save($originalPath.time().$originalImage->getClientOriginalName());
		        $savedNameFull = $originalUrl.time().$originalImage->getClientOriginalName();
		        
		        $originalUrl = '/images/thumbnails/';
		        $originalPath = public_path().$originalUrl;
		        $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());
		        $savedNameThumbnail = $originalUrl.time().$originalImage->getClientOriginalName();

		        $savedName = $originalImage->getClientOriginalName();

		        $save_data = [
		            'image' => $savedName,
		            'image_full' => $savedNameFull,
		            'image_thumbnail' => $savedNameThumbnail,
		            'caption' => "free caption image",
		            'project_id' => $model->id,
		            'created_by' => Auth::id(),
		            'updated_by' => Auth::id(),
		        ];

		        self::create($save_data);
    		}
	    }
	}
    //
}
