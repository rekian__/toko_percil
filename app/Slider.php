<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Slider extends Model{

    protected $fillable = [
		"description",
		"image",
		"caption",
		"url",
        "position",
		"text_style",
        "status",
		"created_by",
		"updated_by",
    ];

    public static $rules = [
		"description" => ["required", "string", "max:255"],
        "position" => ["required"],
        "text_style" => ["required"],
		"image" => ["required"],
		"caption" => ["required", "string", "max:255"],
		"url" => ["required", "string", "max:255"],
		"status" => ["required", "integer"],
    ];

    public static $rules_edit = [
		"description" => ["required", "string", "max:255"],
        "position" => ["required"],
        "text_style" => ["required"],
		"caption" => ["required", "string", "max:255"],
		"url" => ["required", "string", "max:255"],
		"status" => ["required", "integer"],
    ];


    public function getSliders(){
        return $this->orderBy('created_at', 'DESC')->paginate(1);
    }

    public function getSlidersNoPagination(){
        return $this->orderBy('created_at', 'DESC')->get();
    }

    public function getDetailSlider($id){
        return $this->where("id","=",$id)->first();
    }

    public function deleteData($target_route, $model){
        if ($model->delete()) {
            return redirect()->action($target_route);
        }
    }

    public function saveSlider($request, $target_route, $model=null){
        $rules = self::$rules;
    	if ($model) {
	        $rules = self::$rules_edit;
    	}
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

        // if ($model == null) {
        //     $current_timestamp = Carbon::now()->timestamp;
        //     $slug = str_slug($input['name'] . " " . $current_timestamp, "-");
        // } else {
        //     $slug = $model->slug;
        // }
        $savedName = "";
    	if ($request->file('image') != null) {
	        $originalImage = $request->file('image');
	        $fullImage = Image::make($originalImage);
	        $originalUrl = '/images/';
	        $originalPath = public_path().$originalUrl;
	        $fullImage->save($originalPath.time().$originalImage->getClientOriginalName());
	        $savedName = $originalUrl.time().$originalImage->getClientOriginalName();
	    }
        // dd(Auth::id());
		$save_data = [
            'description' => $input['description'],
            'image' => $savedName,
            'caption' => $input['caption'],
            'url' => $input['url'],
            'status' => $input['status'],
            'position' => $input['position'],
            'text_style' => $input['text_style'],
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
        ];
        if ($model && $savedName == "") {
        	unset($save_data["image"]);
        }

	    if ($model == null) {
            $model = self::create($save_data);
        } else {
        	$model->update($save_data);
        }

        if ($model) {
            return redirect()->action($target_route);
        } else {
            return back()->withInput();
        }

    }


}
