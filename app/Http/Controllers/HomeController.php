<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Slider;
use App\ProjectCategory;
use App\Project;
use App\ProductCategory;
use App\Product;
use App\Page;
use Mapper;

class HomeController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $sliders = (new Slider)->getSlidersNoPagination(); 
        $latestProjects = (new Project)->getTopProjects()->get()->take(3);
        $faqs = (new Page)->getPageByPageTypeSlug("home-faq")->get();
        $ourClient = (new Page)->getPageByPageTypeSlug("home-our-client")->first();
        $ourProducts = (new Page)->getPageByPageTypeSlug("icon-produk-kami")->get();
        $ourServices = (new Page)->getPageByPageTypeSlug("icon-layanan-kami")->get();
        $homeInfo = (new Page)->getPageByPageTypeSlug("home-info")->first();
        $homeIcons = (new Page)->getPageByPageTypeSlug("icon-home")->get();
        $product_categories = (new ProductCategory)->getProductCategoriesNoPagination();
        if (count($product_categories) > 6) {
            $product_categories = $product_categories->random(6);
        }
        return view('site/home', compact("sliders", "product_categories", "latestProjects", "faqs", "ourClient", "ourProducts", "ourServices", "homeInfo", "homeIcons"));
    }

    public function product_category(){
        $product_categories = (new ProductCategory)->getProductCategoriesPagination()->paginate(12);
        return view('site/product_category', compact("product_categories"));
    }

    public function product_category_detail($slug){
        $product_category = (new ProductCategory)->getProductCategoriesPagination($slug)->first();
        $related_product_categories = (new ProductCategory)->getProductCategoriesPagination()->get()->random(3);
        return view('site/product_category_detail', compact("product_category", "related_product_categories"));
    }

    public function product_detail($slug){
        $product = (new Product)->getProduct($slug)->first();
        $related_products = (new Product)->getProductByCategory($product->product_category_id)->get()->random(3);
        return view('site/product_detail', compact("product", "related_products"));
    }

    public function project_list(){
        $project_categories = (new ProjectCategory)->getProjectCategoriesPagination()->paginate(6);
        return view('site/project_list', compact("project_categories"));
    }

    public function project_category($slug){
        $projectCategory = (new ProjectCategory)->getProjectCategoriesPDetailBySlug($slug)->first();
        $project_categories = (new ProjectCategory)->getProjectCategoriesOnly()->get();
        return view('site/project_category', compact("project_categories", "projectCategory"));
    }

    public function project_detail($slug){
        $project = (new Project)->getProjectDetailBySlug($slug)->first();
        $project_categories = (new ProjectCategory)->getProjectCategoriesOnly()->get();
        return view('site/project_detail', compact("project_categories", "project"));
    }

    public function blog_list(){
        $posts = (new Post)->getAllPosts()->paginate(10);
        return view('site/blog_list', compact("posts"));
    }

    public function blog_detail($slug){
        $posts = (new Post)->getAllPosts()->get()->random(10);
        $post = (new Post)->getPostDetailBySlug($slug)->first();
        return view('site/blog_detail', compact('post', 'posts'));
    }

    public function kontak_kami(){
        Mapper::map(-6.226117, 106.743565);
        $contactUsIcons = (new Page)->getPageByPageTypeSlug("contact-us-icon")->get();
        return view('site/kontak_kami', compact('contactUsIcons'));
    }

    public function about_us(){
        $aboutUsTeams = (new Page)->getPageByPageTypeSlug("about-us-team")->get();
        $aboutUsInfo1 = (new Page)->getPageByPageTypeSlug("about-us-info-1")->first();
        $aboutUsInfo2 = (new Page)->getPageByPageTypeSlug("about-us-info-2")->first();
        return view('site/about_us', compact('aboutUsTeams', 'aboutUsInfo1', 'aboutUsInfo2'));
    }
}
