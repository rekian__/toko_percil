<?php

namespace App\Http\Controllers\Admin;

use App\ProjectCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectCategoryController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $project_categories = (new ProjectCategory)->getProjectCategories();
        return view('admin/project_category/index', compact("project_categories"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin/project_category/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        return (new ProjectCategory)->saveProjectCategory($request, "Admin\ProjectCategoryController@index");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProjectCategory  $projectCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectCategory $projectCategory){
        $project_category = (new ProjectCategory)->getDetailProjectCategory($projectCategory->id);
        if($project_category == null){
            return redirect()->action('Admin\ProjectCategoryController@index');
        }
        return view('admin/project_category/detail', compact("project_category"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectCategory  $projectCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectCategory $projectCategory){
        $project_category = (new ProjectCategory)->getDetailProjectCategory($projectCategory->id);
        if($project_category == null){
            return redirect()->action('Admin\ProjectCategoryController@index');
        }
        return view('admin/project_category/create', compact("project_category"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectCategory  $projectCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectCategory $projectCategory){
        $project_category = (new ProjectCategory)->getDetailProjectCategory($projectCategory->id);
        return (new ProjectCategory)->saveProjectCategory($request, "Admin\ProjectCategoryController@index", $project_category); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectCategory  $projectCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectCategory $projectCategory){
        return (new ProjectCategory)->deleteData("Admin\ProjectCategoryController@index", $projectCategory);
    }
}
