<?php

namespace App\Http\Controllers\Admin;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $sliders = (new Slider)->getSliders(); 
        return view('admin/slider/index', compact("sliders"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $positions = [
            "left" => "Left",
            "center" => "Center",
            "right" => "Right"
        ];
        return view('admin/slider/create', compact("positions"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        return (new Slider)->saveSlider($request, "Admin\SliderController@index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider){
        $slider = (new Slider)->getDetailSlider($slider->id);
        if($slider == null){
            return redirect()->action('Admin\SliderController@index');
        }
        return view('admin/slider/detail', compact("slider"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider){
        $positions = [
            "left" => "Left",
            "center" => "Center",
            "right" => "Right"
        ];
        $slider = (new Slider)->getDetailSlider($slider->id);
        if($slider == null){
            return redirect()->action('Admin\SliderController@index');
        }
        return view('admin/slider/create', compact("slider", "positions"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider){
        $slider = (new Slider)->getDetailSlider($slider->id);
        return (new Slider)->saveSlider($request, "Admin\SliderController@index", $slider); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider){
        return (new Slider)->deleteData("Admin\SliderController@index", $slider);
    }
}
