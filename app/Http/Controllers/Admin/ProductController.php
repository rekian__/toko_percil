<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $products = (new Product)->getProducts();
        return view('admin/product/index', compact("products"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $product_categories = (new ProductCategory)->getProductCategoriesNoPagination();
        return view('admin/product/create', compact("product_categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        return (new Product)->saveProduct($request, "Admin\ProductController@index");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product){
        $product = (new Product)->getDetailProduct($product->id);
        if($product == null){
            return redirect()->action('Admin\ProductController@index');
        }
        return view('admin/product/detail', compact("product"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product){
        $product_categories = (new ProductCategory)->getProductCategoriesNoPagination();
        $product = (new Product)->getDetailProduct($product->id);
        if($product == null){
            return redirect()->action('Admin\ProductController@index');
        }
        return view('admin/product/create', compact("product", "product_categories"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product){
        $product = (new Product)->getDetailProduct($product->id);
        return (new Product)->saveProduct($request, "Admin\ProductController@index", $product); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product){
        return (new Product)->deleteData("Admin\ProductController@index", $product);
    }
}
