<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use App\PageType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $pages = (new Page)->getPages();
        return view('admin/page/index', compact("pages"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $page_types = (new PageType)->getPageTypeNoPagination();
        return view('admin/page/create', compact("page_types"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        return (new Page)->savePage($request, "Admin\PageController@index");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page){
        $page = (new Page)->getDetailPage($page->id);
        if($page == null){
            return redirect()->action('Admin\PageController@index');
        }
        return view('admin/page/detail', compact("page"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page){
        $page_types = (new PageType)->getPageTypeNoPagination();
        $page = (new Page)->getDetailPage($page->id);
        $listed_forms = [];
        if (isset($page)) {
            $listed_forms = explode(",", $page->pageType->form_element);
        }
        if($page == null){
            return redirect()->action('Admin\PageController@index');
        }
        return view('admin/page/create', compact("page", "page_types", "listed_forms"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page){
        $page = (new Page)->getDetailPage($page->id);
        return (new Page)->savePage($request, "Admin\PageController@index", $page); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page){
        return (new Page)->deleteData("Admin\PageController@index", $page);
    }
}
