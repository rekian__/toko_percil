<?php

namespace App\Http\Controllers\Admin;

use App\PageType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageTypeController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $page_types = (new PageType)->getPageTypes();
        return view('admin/page_type/index', compact("page_types"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin/page_type/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        return (new PageType)->savePageType($request, "Admin\PageTypeController@index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PageType  $pageType
     * @return \Illuminate\Http\Response
     */
    public function show(PageType $pageType){
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PageType  $pageType
     * @return \Illuminate\Http\Response
     */
    public function edit(PageType $pageType){
        $page_type = (new PageType)->getDetailPageType($pageType->id);
        if($page_type == null){
            return redirect()->action('Admin\PageTypeController@index');
        }
        return view('admin/page_type/create', compact("page_type"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PageType  $pageType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PageType $pageType){
        $page_type = (new PageType)->getDetailPageType($pageType->id);
        return (new PageType)->savePageType($request, "Admin\PageTypeController@index", $page_type); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PageType  $pageType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageType $pageType){
        return (new PageType)->deleteData("Admin\PageTypeController@index", $pageType);
    }
}
