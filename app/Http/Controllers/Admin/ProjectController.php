<?php

namespace App\Http\Controllers\Admin;

use App\Project;
use App\ProjectCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $projects = (new Project)->getProjects();
        return view('admin/project/index', compact("projects"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $project_categories = (new ProjectCategory)->getProjectCategoriesNoPagination();
        return view('admin/project/create', compact("project_categories"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        return (new Project)->saveProject($request, "Admin\ProjectController@index");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project){
        $project = (new Project)->getDetailProject($project->id);
        if($project == null){
            return redirect()->action('Admin\ProjectController@index');
        }
        return view('admin/project/detail', compact("project"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project){
        $project_categories = (new ProjectCategory)->getProjectCategoriesNoPagination();
        $project = (new Project)->getDetailProject($project->id);
        if($project == null){
            return redirect()->action('Admin\ProjectController@index');
        }
        return view('admin/project/create', compact("project", "project_categories"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project){
        $project = (new Project)->getDetailProject($project->id);
        return (new Project)->saveProject($request, "Admin\ProjectController@index", $project); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project){
        return (new Project)->deleteData("Admin\ProjectController@index", $project);
    }
}
