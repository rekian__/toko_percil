<?php

namespace App\Http\Controllers\Admin;

use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductCategoryController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $product_categories = (new ProductCategory)->getProductCategories();
        return view('admin/product_category/index', compact("product_categories"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('admin/product_category/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        return (new ProductCategory)->saveProductCategory($request, "Admin\ProductCategoryController@index");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory){
        $product_category = (new ProductCategory)->getDetailProductCategory($productCategory->id);
        if($product_category == null){
            return redirect()->action('Admin\ProductCategoryController@index');
        }
        return view('admin/product_category/detail', compact("product_category"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategory $productCategory){
        $product_category = (new ProductCategory)->getDetailProductCategory($productCategory->id);
        if($product_category == null){
            return redirect()->action('Admin\ProductCategoryController@index');
        }
        return view('admin/product_category/create', compact("product_category"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCategory $productCategory){
        $product_category = (new ProductCategory)->getDetailProductCategory($productCategory->id);
        return (new ProductCategory)->saveProductCategory($request, "Admin\ProductCategoryController@index", $product_category); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategory $productCategory){
        return (new ProductCategory)->deleteData("Admin\ProductCategoryController@index", $productCategory);
    }
}
