<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProjectImage;
use Image;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Project extends Model{

	public function projectImages() {
        return $this->hasMany('App\ProjectImage');
    }

	public function projectCategory() {
        return $this->belongsTo('App\ProjectCategory');
    }

    protected $fillable = [
		"id",
		"title",
		"slug",
		"description",
		"content",
		"status",
		"project_category_id",
		"created_by",
    ];

    public static $rules = [
		"title" => ["required", "string", "max:255"],
		"description" => ["required", "string", "max:255"],
		"content" => ["required", "string"],
		"status" => ["required", "string", "max:255"],
		"project_category_id" => ["required", "integer"],
    ];


    public function getProjects(){
        return $this->orderBy('created_at', 'DESC')->paginate(1);
    }

    public function getTopProjects(){
        return $this->orderBy('created_at', 'DESC');
    }

    public function getDetailProject($id){
        return $this->with("projectImages")->where("id","=",$id)->first();
    }

    public function getProjectDetailBySlug($slug){
        return $this->with("projectImages")->where("slug","=",$slug);
    }

    public function deleteData($target_route, $model){
        if ($model->delete()) {
            return redirect()->action($target_route);
        }
    }

    public function saveProject($request, $target_route, $model=null){
        $rules = self::$rules;
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
        	// dd($validator);
            return back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

		if ($model == null) {
		    $current_timestamp = Carbon::now()->timestamp;
		    $slug = str_slug($current_timestamp . " " . $input['title'], "-");
		} else {
		    $slug = $model->slug;
		}

		$save_data = [
            'title' => $input['title'],
            'slug' => $slug,
            'description' => $input['description'],
            'content' => $input['content'],
            'status' => $input['status'],
            'project_category_id' => $input['project_category_id'],
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
        ];

	    if ($model == null) {
            $model = self::create($save_data);
        } else {
        	$model->update($save_data);
        }


        if ($model) {
        	(new ProjectImage)->saveModelImage($request, $model);
            return redirect()->action($target_route);
        } else {
            return back()->withInput();
        }

    }



}
