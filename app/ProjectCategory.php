<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ProjectCategory extends Model{

    public function projects() {
        return $this->hasMany('App\Project');
    }

    public function projectsTop3() {
        return $this->projects()->take(3);
    }

    protected $fillable = [
		"id",
		"title",
		"slug",
		"description",
		"content",
		"image",
		"image_thumbnail",
		"image_full",
		"caption",
		"status",
		"created_by",
    ];

    public static $rules = [
		"title" => ["required", "string", "max:255"],
		"description" => ["required", "string", "max:255"],
		"content" => ["required", "string"],
		"image" => ["required"],
		"caption" => ["required", "string", "max:255"],
		"status" => ["required", "string", "max:255"],
    ];

    public static $rules_edit = [
		"title" => ["required", "string", "max:255"],
		"description" => ["required", "string", "max:255"],
		"content" => ["required", "string"],
		"caption" => ["required", "string", "max:255"],
		"status" => ["required", "string", "max:255"],
    ];


    public function getProjectCategories(){
        return $this->orderBy('created_at', 'DESC')->paginate(1);
    }

    public function getProjectCategoriesNoPagination(){
        return $this->orderBy('created_at', 'DESC')->get();
    }

    public function getProjectCategoriesPagination(){
        return $this->with("projects", "projects.projectImages")->orderBy('id', 'DESC');
    }

    public function getProjectCategoriesPDetailBySlug($slug){
        return $this->with("projects", "projects.projectImages")->where("slug", $slug);
    }

    public function getProjectCategoriesOnly(){
        return $this->orderBy('id', 'DESC');
    }

    public function getDetailProjectCategory($id){
        return $this->where("id","=",$id)->first();
    }

    public function deleteData($target_route, $model){
        if ($model->delete()) {
            return redirect()->action($target_route);
        }
    }

    public function saveProjectCategory($request, $target_route, $model=null){
        $rules = self::$rules;
    	if ($model) {
	        $rules = self::$rules_edit;
    	}
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

        $savedNameFull = "";
        $savedNameThumbnail = "";
        $savedName = "";
    	if ($request->file('image') != null) {
        
	        $originalImage = $request->file('image');

	        $fullImage = Image::make($originalImage);
	        $thumbnailImage = Image::make($originalImage)->resize(315, 156);
	        
	        $originalUrl = '/images/';
	        $originalPath = public_path().$originalUrl;
	        $fullImage->save($originalPath.time().$originalImage->getClientOriginalName());
	        $savedNameFull = $originalUrl.time().$originalImage->getClientOriginalName();
	        
	        $originalUrl = '/images/thumbnails/';
    		if (!file_exists(public_path().$originalUrl)) {
	            mkdir(public_path().$originalUrl, 666, true);
	        }
	        $originalPath = public_path().$originalUrl;
	        $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());
	        $savedNameThumbnail = $originalUrl.time().$originalImage->getClientOriginalName();

	        $savedName = $originalImage->getClientOriginalName();
	    }
        // dd(Auth::id());
		if ($model == null) {
		    $current_timestamp = Carbon::now()->timestamp;
		    $slug = str_slug($current_timestamp . " " . $input['title'], "-");
		} else {
		    $slug = $model->slug;
		}

		$save_data = [
            'title' => $input['title'],
            'slug' => $slug,
            'description' => $input['description'],
            'content' => $input['content'],
            'image' => $savedName,
            'image_full' => $savedNameFull,
            'image_thumbnail' => $savedNameThumbnail,
            'caption' => $input['caption'],
            'status' => $input['status'],
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
        ];
        if ($model && $savedNameFull == "") {
        	unset($save_data["image"]);
        	unset($save_data["image_full"]);
        	unset($save_data["image_thumbnail"]);
        }

	    if ($model == null) {
            $model = self::create($save_data);
        } else {
        	$model->update($save_data);
        }

        if ($model) {
            return redirect()->action($target_route);
        } else {
            return back()->withInput();
        }

    }


}
