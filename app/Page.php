<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PageImage;
use Image;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Page extends Model{

	public function pageImages() {
        return $this->hasMany('App\PageImage');
    }

	public function pageType() {
        return $this->belongsTo('App\PageType');
    }

    protected $fillable = [
		"id",
		"title",
		"slug",
		"description",
		"content",
        "image",
        "image_thumbnail",
        "image_full",
        "caption",
		"status",
		"page_type_id",
		"created_by",
    ];

    public static $rules = [];

    public function getHeaderLogo(){
        return $this->where("id",1);
    }

    public function getPageByPageTypeSlug($pageTypeSlug){
        return $this->whereHas("pageType", function($q) use ($pageTypeSlug){
            $q->where('slug', $pageTypeSlug);
        });
    }

    public function getPages(){
        return $this->orderBy('created_at', 'DESC')->paginate(20);
    }

    public function getDetailPage($id){
        return $this->with("pageImages")->where("id","=",$id)->first();
    }

    public function getPageDetailBySlug($slug){
        return $this->with("pageImages")->where("slug","=",$slug);
    }

    public function deleteData($target_route, $model){
        if ($model->delete()) {
            return redirect()->action($target_route);
        }
    }

    public function savePage($request, $target_route, $model=null){
        $rules = self::$rules;
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
        	// dd($validator);
            return back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

        $savedNameFull = "";
        $savedNameThumbnail = "";
        $savedName = "";
        if ($request->file('image') != null) {
        
            $originalImage = $request->file('image');

            $fullImage = Image::make($originalImage);
            $thumbnailImage = Image::make($originalImage)->resize(200, 200);
            
            $originalUrl = '/images/';
            $originalPath = public_path().$originalUrl;
            $fullImage->save($originalPath.time().$originalImage->getClientOriginalName());
            $savedNameFull = $originalUrl.time().$originalImage->getClientOriginalName();
            
            $originalUrl = '/images/thumbnails/';
            if (!file_exists(public_path().$originalUrl)) {
                mkdir(public_path().$originalUrl, 666, true);
            }
            $originalPath = public_path().$originalUrl;
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());
            $savedNameThumbnail = $originalUrl.time().$originalImage->getClientOriginalName();

            $savedName = $originalImage->getClientOriginalName();
        }


		if ($model == null) {
		    $current_timestamp = Carbon::now()->timestamp;
		    $slug = str_slug($current_timestamp . " " . $input['title'], "-");
		} else {
		    $slug = $model->slug;
		}

		$save_data = $input;
        $save_data['slug'] = $slug;
        $save_data['created_by'] = Auth::id();
        $save_data['updated_by'] = Auth::id();
        $save_data['image'] = $savedName;
        $save_data['image_full'] = $savedNameFull;
        $save_data['image_thumbnail'] = $savedNameThumbnail;

        if ($model && $savedNameFull == "") {
            unset($save_data["image"]);
            unset($save_data["image_full"]);
            unset($save_data["image_thumbnail"]);
        }

	    if ($model == null) {
            $model = self::create($save_data);
        } else {
        	$model->update($save_data);
        }


        if ($model) {
        	(new PageImage)->saveModelImage($request, $model);
            return redirect()->action($target_route);
        } else {
            return back()->withInput();
        }

    }



}
