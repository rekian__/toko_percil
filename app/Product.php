<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProductImage;
use Image;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Product extends Model{

	public function productImages() {
        return $this->hasMany('App\ProductImage');
    }

	public function productCategory() {
        return $this->belongsTo('App\ProductCategory');
    }

    protected $fillable = [
		"id",
		"title",
		"slug",
		"description",
        "content",
		"spesification",
		"status",
		"product_category_id",
		"created_by",
    ];

    public static $rules = [
		"title" => ["required", "string", "max:255"],
		"description" => ["required", "string", "max:255"],
        "content" => ["required", "string"],
		"spesification" => ["required", "string"],
		"status" => ["required", "string", "max:255"],
		"product_category_id" => ["required", "integer"],
    ];


    public function getProducts(){
        return $this->orderBy('created_at', 'DESC')->paginate(1);
    }

    public function getDetailProduct($id){
        return $this->where("id","=",$id)->first();
    }

    public function getProduct($slug){
        return $this->where("status", "1")->where("slug","=",$slug);
    }

    public function getProductByCategory($productCategoryId){
        return $this->where("status", "1")->where("product_category_id","=",$productCategoryId);
    }

    public function deleteData($target_route, $model){
        if ($model->delete()) {
            return redirect()->action($target_route);
        }
    }

    public function saveProduct($request, $target_route, $model=null){
        $rules = self::$rules;
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
        	// dd($validator);
            return back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

		if ($model == null) {
		    $current_timestamp = Carbon::now()->timestamp;
		    $slug = str_slug($current_timestamp . " " . $input['title'], "-");
		} else {
		    $slug = $model->slug;
		}

		$save_data = [
            'title' => $input['title'],
            'slug' => $slug,
            'description' => $input['description'],
            'content' => $input['content'],
            'spesification' => $input['spesification'],
            'status' => $input['status'],
            'product_category_id' => $input['product_category_id'],
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
        ];

	    if ($model == null) {
            $model = self::create($save_data);
        } else {
        	$model->update($save_data);
        }


        if ($model) {
        	(new ProductImage)->saveModelImage($request, $model);
            return redirect()->action($target_route);
        } else {
            return back()->withInput();
        }

    }



}
