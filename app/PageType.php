<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use Illuminate\Support\Facades\Auth;

class PageType extends Model{

	public function pages() {
        return $this->hasMany('App\Page');
    }

    public function getPageTypes(){
        return $this->orderBy('created_at', 'DESC')->paginate(10);
    }

    protected $fillable = [
		"title",
        "slug",
		"form_element",
		"status",
		"created_by",
    ];

    public static $rules = [
		"title" => ["required", "string", "max:255"],
		"status" => ["required", "string", "max:255"],
    ];

    public function getDetailPageType($id){
        return $this->where("id","=",$id)->first();
    }

    public function deleteData($target_route, $model){
        if ($model->delete()) {
            return redirect()->action($target_route);
        }
    }

    public function getPageTypeNoPagination(){
        return $this->orderBy('created_at', 'DESC')->get();
    }

    public function savePageType($request, $target_route, $model=null){
        $rules = self::$rules;
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $input = $request->all();

        // dd(Auth::id());
		if ($model == null) {
		    $slug = str_slug($input['title'], "-");
		} else {
		    $slug = $model->slug;
		}

		$save_data = [
            'title' => $input['title'],
            'slug' => $slug,
            'form_element' => $input['form_element'],
            'status' => $input['status'],
            'created_by' => Auth::id(),
            'updated_by' => Auth::id(),
        ];

	    if ($model == null) {
            $model = self::create($save_data);
        } else {
        	$model->update($save_data);
        }

        if ($model) {
            return redirect()->action($target_route);
        } else {
            return back()->withInput();
        }

    }
}
