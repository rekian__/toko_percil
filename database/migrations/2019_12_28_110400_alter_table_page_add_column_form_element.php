<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePageAddColumnFormElement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('page_types', function (Blueprint $table) {
            $table->text('form_element')->nullable()->after("slug");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('page_types', function (Blueprint $table) {
            $table->dropColumn('form_element');
        });
    }
}
