<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSliderAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('sliders', function (Blueprint $table) {
            $table->enum('position', ['left', 'center', 'right']);
            $table->integer('text_style');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('sliders', function (Blueprint $table) {
            $table->dropColumn('position');
            $table->dropColumn('text_style');
        });
    }
}
