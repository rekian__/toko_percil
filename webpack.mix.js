const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/tinymce.min.js', 'public/js')
	.js('resources/site/js/creative.js', 'public/site/js')
	.js('resources/site/vendor_custom/jquery/jquery.min.js', 'public/site/vendor/jquery')
	.js('resources/site/vendor_custom/bootstrap/js/bootstrap.bundle.min.js', 'public/site/vendor/bootstrap/js')
	.js('resources/site/vendor_custom/jquery-easing/jquery.easing.min.js', 'public/site/vendor/jquery-easing')
	.copy('resources/site/vendor_custom/magnific-popup/jquery.magnific-popup.js', 'public/site/vendor/magnific-popup')
    .copy('resources/site/vendor_custom/fontawesome-free', 'public/site/vendor/fontawesome-free')
    .copy('resources/site/vendor_custom/magnific-popup/magnific-popup.css', 'public/site/vendor/magnific-popup')
    .sass('resources/site/scss/creative.scss', 'public/site/css')
    .sass('node_modules/admin-lte/build/scss/AdminLTE.scss', 'public/css')
    .copy('node_modules/admin-lte/dist/js/adminlte.js', 'public/js')
    .copy('node_modules/admin-lte/dist/img', 'public/dist/img')
    .copy('resources/site/img', 'public/site/img')
    .copy('resources/site/logo', 'public/site/logo')
	.copy('node_modules/slick-carousel/slick/slick.js', 'public/js')
    .sass('node_modules/slick-carousel/slick/slick-theme.scss', 'public/css')
    .sass('node_modules/slick-carousel/slick/slick.scss', 'public/css')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/style.scss', 'public/css');
