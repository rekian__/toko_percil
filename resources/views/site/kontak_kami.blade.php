@extends('site.layouts.base_alt')

@section('meta_section')
    <title>Segala Tenda Untuk Event Anda | Agung Tent</title>
    <meta name="title" content=""/>
    <meta name="description" content=""/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="article:publisher" content="" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:site" content="@Shelter_Tents" />
    <meta name="twitter:creator" content="@Shelter_Tents" />
@endsection

@section('content')
    <section class="page-section" id="product">
        <div class="about-us" >
            {!! Mapper::render() !!}
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-justify">
                    <h2 class="mt-0 text-center">Kami Berbasis di Banten, Indonesia</h2>
                    <hr class="divider my-4">
                    <div class="row">
                        @foreach($contactUsIcons as $contactUsIcon)
                            <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                                <div class="mt-5">
                                    <img class="img-fluid mb-4 fa-image-size" src="{{ $contactUsIcon->image_thumbnail }}">
                                    <div class="text-muted mb-0">{!! $contactUsIcon->content !!}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="page-section-hide page-section bg-primary" id="product">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-justify">
                    <h2 class="mt-0 text-center text-white">Inquiry Form</h2>
                    <hr class="divider my-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <form>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nama" name="nama" required="true">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="email" name="email" required="true">
                                </div>
                                <div class="form-group">
                                    <input type="phone" class="form-control" placeholder="Telepon" name="phone" required="true">
                                </div>
                                <div class="form-group">
                                    <textarea name="content" placeholder="inquiry anda" class="form-control" required="true"></textarea>
                                </div>
                                <button type="submit" class="btn col-lg-12 btn btn-secondary">Submit</button>
                            </form>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </section>
@endsection
