@extends('site.layouts.base_alt')

@section('meta_section')
    <title>Segala Tenda Untuk Event Anda | Agung Tent</title>
    <meta name="title" content=""/>
    <meta name="description" content=""/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="article:publisher" content="" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:site" content="@Shelter_Tents" />
    <meta name="twitter:creator" content="@Shelter_Tents" />
@endsection

@section('content')
    <section class="page-section page-section-2" id="product">
        <div class="container">
            <div class="row justify-content-center mb-4">
                <div class="col-lg-12 text-justify">
                    <h2 class="mt-0 text-center">Daftar Projek</h2>
                    <hr class="divider my-4">
                    <div class="row mb-4">
                        <div class="col-lg-12" id="portfolio">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    @foreach($project_categories as $project_category)
    		                        	<div class="col-lg-2 col-md-4 col-sm-6 mb-2">
    			                        	<a class="scroll-me" data-target="{{ $project_category->slug }}">
    			                        		<nav class="col-lg-12 btn btn-primary">{{ $project_category->title }}</nav>
    			                        	</a>
    		                        	</div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                	</div>
                    @foreach($project_categories as $project_category)
                        <div class="row mt-4 {{ $project_category->slug }}">
                            <div class="col-lg-12" id="portfolio">
                                <div class="container-fluid p-0">
                                	<h3 >{{ $project_category->title }}</h3>
                                	<hr class="h3-divider pb-1">
                                    <div class="row">
                                        @if(isset( $project_category->projects) && count($project_category->projects)    > 0)
                                            @foreach($project_category->projects as $project)
                                                <div class="col-lg-4 col-md-12 col-sm-12 mb-4">
                                                    <a class="portfolio-box" href="{{ route('project_detail', $project->slug) }}">
                                                        <div class="row">
                                                            <img class="col-lg-12 img-fluid" src="{{ isset($project->projectImages[0]) ? $project->projectImages[0]->image_thumbnail : asset('site/img/portfolio/thumbnails/1.jpg') }}" alt="">
                                                        </div>
                                                        <div class="portfolio-box-caption p-3">
                                                            <div class="project-category text-white-50">
                                                                {{ $project_category->title }}
                                                            </div>
                                                            <div class="project-name btn-blue-cover mt-2">
                                                                <span class="btn btn-blue text-white">
                                                                    Gallery and Spesification
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a href="{{ route('project_detail', $project->slug) }}">
                                                    	<h4 class="small mt-4 text-muted">{{ $project->title }}</h4>
                                                    </a>
                                                    <div class="content small text-muted">
                                                    	<p>{{ $project->description }} <a href="{{ route('project_detail', $project->slug) }}">readmore</a></p>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="col-lg-12 col-md-12 col-sm-12 mb-4 text-center">
                                                <p>Data Tidak Tersedia</p>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination text-center">
                            {{ $project_categories->links() }}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("extra_js")
    <script type="text/javascript">
        $(document).ready(function(){
            $(this).find(".scroll-me").on("click", function(){
                var target = $(this).data("target");
                var scrollClass = "."+target;
                $('html,body').animate({
                    scrollTop: $(scrollClass).offset().top-100
                },'slow');
            })
        })
    </script>
@endsection
