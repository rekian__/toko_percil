@extends('site.layouts.base_alt')

@section('meta_section')
    <title>Segala Tenda Untuk Event Anda | Agung Tent</title>
    <meta name="title" content=""/>
    <meta name="description" content=""/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="article:publisher" content="" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:site" content="@Shelter_Tents" />
    <meta name="twitter:creator" content="@Shelter_Tents" />
@endsection

@section('content')
    <section class="page-section page-section-2" id="product">
        <div class="container">
            <div class="row justify-content-center mb-4">
                <div class="col-lg-12 text-justify">
                    @foreach($posts as $post)
                        <div class="mb-4 article-list">
                            <div class="row">
                                <div class="col-lg-4">
                                    <a href="#" class="row thumbnail">
                                        <img class="img-fluid col-lg-12" src="{{ $post->image_thumbnail }}">
                                    </a>
                                </div>
                                <div class="col-lg-8 content-cover">
                                    <a href="{{ route('blog_detail',  $post->slug) }}" class="mb-4">
                                        <h3 class="mb-0">{{ $post->title }}</h3>
                                        <i class="text-muted small">{{ date('d-m-Y', strtotime($post->created_at)) }}</i>
                                    </a>
                                    <div class="clearfix"></div>
                                    <div class="content text-muted mt-2">
                                        <p>{{ $post->description }} <a href="#">readmore</a> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination text-center">
                            {{ $posts->links() }}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
@endsection
