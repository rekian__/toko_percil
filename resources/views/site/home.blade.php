@extends('site.layouts.base')

@section('meta_section')
    <title>Segala Tenda Untuk Event Anda | Agung Tent</title>
    <meta name="title" content=""/>
    <meta name="description" content=""/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Beranda | Agung Tent | Persewaan dan Jual Beli Tenda" />
    <meta property="og:url" content="{{ route('home') }}" />
    <meta property="og:site_name" content="agungtent.com" />
    <meta property="og:image" content="{{ route('home') }}{{ count($sliders) >= 1 ? $sliders[0]->image : '' }}" />
    <meta property="article:publisher" content="Agung Tent Admin" />
    <meta name="twitter:card" content="{{ route('home') }}{{ count($sliders) >= 1 ? $sliders[0]->image : '' }}" />
    <meta name="twitter:description" content="Beranda | Agung Tent | Persewaan dan Jual Beli Tenda" />
    <meta name="twitter:title" content="Beranda | Agung Tent | Persewaan dan Jual Beli Tenda" />
    <meta name="twitter:site" content="@agung_tent" />
    <meta name="twitter:creator" content="@agung_tent" />
@endsection

@section('slider')
    @foreach($sliders as $slider)
        <header class="masthead" style="background: -webkit-gradient(linear, left top, left bottom, from(#00000088), to(#00000088)), url({{ $slider->image }});background: linear-gradient(to bottom, #00000088 0%, #00000088 100%), url({{ $slider->image }}); background-size: cover; background-repeat: no-repeat;">
            <div class="col-lg-12 h-100">
                <div class="row h-100 align-items-center justify-content-center text-{{ $slider->position }} style-{{ $slider->text_style }}">
                    <div class="col-lg-10 align-self-end">
                        <h1 class="text-uppercase text-white font-weight-bold">{{ $slider->caption }}</h1>
                        <hr class="divider my-4">
                        <p class="text-white-75 font-weight-light mb-5">{{ $slider->description }}</p>
                        <a class="btn btn-primary btn-xl js-scroll-trigger" href="{{ $slider->url }}">Find Out More</a>
                    </div>
                    <div class="col-lg-8 align-self-baseline">
                    </div>
                </div>
            </div>
        </header>
    @endforeach
@endsection

@section('content')

    <section class="page-section-hide page-section bg-grey">
        <div class="container">
            <h2 class="text-center mt-0">Layanan Kami</h2>
            <hr class="divider my-4">
            <div class="row">
                @foreach($ourServices as $ourService)
                    <div class="col-lg-6 col-md-6 text-center">
                        <div class="mt-5">
                            <img class="img-fluid mb-4" src="{{ $ourService->image_thumbnail }}">
                            <h3 class="h4 mb-2">{{ $ourService->title }}</h3>
                            <p class="mb-0 text-muted">{{ $ourService->description }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="page-section-hide page-section">
        <div class="container">
            <h2 class="text-center mt-0">Produk Kami</h2>
            <hr class="divider my-4">
            <div class="row">
                @foreach($ourProducts as $ourProduct)
                    <div class="col-lg-4 col-md-4 col-sm-12 text-center">
                        <div class="mt-5">
                            <img class="img-fluid mb-4" src="{{ $ourProduct->image_thumbnail }}">
                            <h3 class="h4 mb-2">{{ $ourProduct->title }}</h3>
                            <p class="text-muted mb-0">{{ $ourProduct->description }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="page-section-hide page-section bg-grey" id="about">
        <div class="container">
            @if(isset($homeInfo))
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-12 pr-4">
                        <h2 class="mt-0 mb-4"> {{ $homeInfo->title }}</h2>
                        <div class="">{!! $homeInfo->content !!}</div>
                    </div>
                    <div class="col-lg-6 col-md-12 text-center pl-4">
                        <img class="img-fluid" src="{{ $homeInfo->image_full }}">
                    </div>
                </div>
            @endif
        </div>
    </section>

    <!-- Portfolio Section -->
    <section id="portfolio">
        <div class="container-fluid p-0">
            <div class="row no-gutters">
                @foreach($product_categories as $product_category)
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="#">
                            <div class="row">
                                <img class="img-fluid col-lg-12" src="{{ $product_category->image_thumbnail }}" alt="">
                            </div>
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">
                                </div>
                                <div class="project-name">
                                    {{ $product_category->title }}
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
                <div class="col-lg-12 col-sm-12">
                    <a class="col-lg-12 btn btn-primary" href="{{ route('product_category') }}" alt="Kategori Produk Agung tent" title="Kategori Produk Agung tent">Temukan Lebih Banyak</a>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section class="page-section-hide page-section" id="contact">
        <div class="container mb-4">
            <div class="row">
                @foreach($homeIcons as $homeIcon)
                    <div class="col-lg-4 col-md-12 text-center">
                        <div class="mt-1">
                            <img class="fa-image-size mb-2" src="{{ $homeIcon->image_thumbnail }}">
                            <h3 class="small h4 mb-4">{{ $homeIcon->caption }}</h3>
                            @if($homeIcon->caption == "Tentang Kami")
                                <div class="row text-justify">
                                    <div class="col-lg-12">
                                        {!! $homeAboutUs->content !!} <p class="small text-muted mb-4" ><a href="{{ route('about_us') }}">readmore</a></p>
                                    </div>
                                </div>
                            @elseif($homeIcon->caption == "Acara Terkini")
                                <div class="row text-justify">
                                    @foreach($latestProjects as $latestProject)
                                        <div class="col-lg-12">
                                            <h3 class="small h4 mb-2">{{ $latestProject->title }}</h3>
                                            <p class="small text-muted mb-4">{{ $latestProject->description }} <a href="{{ route('project_detail', $latestProject->slug) }}" >readmore</a></p>
                                        </div>
                                    @endforeach
                                </div>
                            @elseif($homeIcon->caption == "Mengapa Memilih Kami")
                                <div class="row text-justify">
                                    <div class="accordion smallsize col-lg-12" id="accordionExample">
                                        @foreach($faqs as $key => $faq)
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <button class="small text-muted btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $faq->slug }}" aria-expanded="true" aria-controls="collapse{{ $faq->slug }}">
                                                      {{ $faq->title }}
                                                    </button>
                                                </div>

                                                <div id="collapse{{ $faq->slug }}" class="collapse {{ $key == 0 ? 'show' : ''}}" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                    <div class="card-body small text-muted ">
                                                        {!! $faq->content !!}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <h2 class="mt-0">Klien Kami</h2>
                    <hr class="divider my-4">
                    <div class="klien-kami" id="klien-kami">
                        @if(isset($ourClient))
                            @foreach($ourClient->pageImages as $pageImage)
                            <div class="col-lg-12">
                                <img class="img-fluid" src="{{ $pageImage->image_thumbnail }}">
                            </div>
                            @endforeach
                        @else
                            <div class="col-lg-12">
                                <p class="text-center">
                                    data tidak ditemukan
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
