@extends('site.layouts.base_alt')

@section('meta_section')
    <title>Segala Tenda Untuk Event Anda | Agung Tent</title>
    <meta name="title" content=""/>
    <meta name="description" content=""/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="article:publisher" content="" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:site" content="@Shelter_Tents" />
    <meta name="twitter:creator" content="@Shelter_Tents" />
@endsection

@section('content')

    <section class="page-section page-section-2" id="product">
        <div class="container">
            <div class="row justify-content-center mb-4">
                <div class="col-lg-12 text-justify magnific-this">
                    <h2 class="mt-0 text-center mt-4">{{ $project->title }}</h2>
                    <p> {{ $project->description }} </p>
                    <hr class="divider my-4">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <h3 class="h4 mb-4 mt-2">Mungkin Anda Tertarik</h3>
                            <hr>
                            <div class="text-muted mb-4 you-may-like">
	                            <div class="container-fluid p-0">
	                                <div class="row">
                                        @foreach($project_categories as $project_category)
                                            <div class="col-lg-12 col-md-4 col-sm-6 mb-2">
    				                        	<a href="{{ route('project_category', $project_category->slug) }}">
    				                        		<nav class="col-lg-12 btn btn-primary {{ $project_category->slug == $project->projectCategory->slug ? 'active text-white-50' : '' }}"> {{ $project_category->title }}</nav>
    				                        	</a>
    			                        	</div>
                                        @endforeach
	                                </div>
	                            </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12 col-sm-12 text-muted mb-4">
                            <div class="row mb-4" id="for-slick">
                                @if(isset( $project->projectImages) && count($project->projectImages) > 0)
                                    @foreach($project->projectImages as $image)
                                        <img title="{{ $project->title }}" alt="{{ $project->title }}" class="col-lg-12 img-fluid" src="{{ $image->image_thumbnail }}" alt="">
                                    @endforeach
                                @endif
                            </div>
                            {!! $project->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
