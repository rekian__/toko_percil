@extends('site.layouts.base_alt')

@section('meta_section')
    <title>Segala Tenda Untuk Event Anda | Agung Tent</title>
    <meta name="title" content=""/>
    <meta name="description" content=""/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="article:publisher" content="" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:site" content="@Shelter_Tents" />
    <meta name="twitter:creator" content="@Shelter_Tents" />
@endsection

@section('content')
    <section class="page-section" id="product">
        <div class="about-us about-us-background">
            <div class="row text-cover">
                <h1 class="col-lg-12 mt-0 text-center text-white">{{ $aboutUsInfo1->title }}</h1>
                <h2 class="col-lg-12 mt-0 text-center text-white">{{ $aboutUsInfo1->description }}</h2>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center mt-4">
                <div class="col-lg-12 text-center mt-4">
                    <h2 class="mt-0 text-center mt-4">{{ $aboutUsInfo2->title }}</h2>
                    <h2 class="mt-0 text-center">{{ $aboutUsInfo2->description }}</h2>
                    <hr class="divider my-4">
                    {!! $aboutUsInfo2->content !!}
                </div>
            </div>
        </div>
    </section>
    <section class="page-section-hide pt-4 bg-primary about-us-page ">
        <div class="pt-4 container">
            <div class="pt-4 row justify-content-center">
                <div class="col-lg-12 text-justify">
                    <div class="row">
                        <div class="col-lg-4 text-center progressbar-cover">
                            <i class="fa fa-campground text-white"></i>
                            <div class="progressbar progressbar-non-presentage" data-animate="false">
                                <div class="circle" data-percent="38800">
                                    <div class="canvas text-white" data-for-percent="no" thickness="0" data-color="#2f318b00" data-satuan="m2"></div>

                                </div>
                            </div>
                            <span class="text-white">Konstruksi Didirikan</span>
                        </div>
                        <div class="col-lg-4 text-center progressbar-cover">
                            <div class="progressbar progressbar-presentage" data-animate="false">
                                <div class="circle" data-percent="99">
                                    <div class="canvas text-white" data-for-percent="yes" thickness="5" data-color="#2f318b" data-satuan="%"></div>

                                </div>
                            </div>
                            <span class="text-white">Client Puas</span>
                        </div>
                        <div class="col-lg-4 text-center progressbar-cover">
                            <i class="fa fa-user text-white"></i><div class="progressbar progressbar-non-presentage" data-animate="false">
                                <div class="circle" data-percent="90">
                                    <div class="canvas text-white" data-for-percent="no" thickness="0" data-color="#2f318b00"></div>

                                </div>
                            </div>
                            <span class="text-white">Projek Selesai</span>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </section>
    <section class="page-section-hide page-section">
        <div class="container">
            <h2 class="text-center mt-0">Team Kami</h2>
            <hr class="divider mt-4">
            <div class="row">
                @foreach($aboutUsTeams as $aboutUsTeam)
                    <div class="mt-2 col-lg-4 col-md-4 col-sm-12 text-center">
                        <div class="card col-lg-12 col-md-12 col-sm-12 text-center">
                            <img src="{{ $aboutUsTeam->image_full }}" class="mt-3 card-img-top img-fluid" alt="{{ $aboutUsTeam->description }} title="{{ $aboutUsTeam->description }}">
                            <div class="card-body">
                                <h5 class="card-title">{{ $aboutUsTeam->title }}</h5>
                                <p class="card-text">{{ $aboutUsTeam->description }}</p>
                                <a href="{{ $aboutUsTeam->caption }}" alt="{{ $aboutUsTeam->description }}" title="{{ $aboutUsTeam->description }}" class="btn btn-primary">Linkedin</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
@section("extra_js")
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-circle-progress/1.2.2/circle-progress.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function ($) {
            function animateElements() {
                $('.progressbar').each(function () {
                    var elementPos = $(this).offset().top;
                    var topOfWindow = $(window).scrollTop();
                    var percent = $(this).find('.circle').attr('data-percent');
                    var color = $(this).find('.canvas').data('color');
                    var thickness = $(this).find('.canvas').data('thickness');
                    var for_percent = $(this).find('.canvas').data('for-percent');
                    var percentage = parseInt(percent, 10) / parseInt(100, 10);
                    var animate = $(this).data('animate');
                    console.log(for_percent);
                    if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
                        if (for_percent === "yes"){
                            var percent_data = percent / 100;
                        } else {
                            var percent_data = percent;
                        }
                        $(this).data('animate', true);
                        $(this).find('.circle').circleProgress({
                            startAngle: -Math.PI / 2,
                            value: percent_data,
                            size: 180,
                            thickness: thickness,
                            emptyFill: "rgba(0,0,0, .2)",
                            fill: {
                                color: color
                            }
                        }).on('circle-animation-progress', function (event, progress, stepValue) {
                            var attribute = $(this).find('div').attr("data-satuan");
                            if (for_percent === "yes"){
                                var stepValue = stepValue * 100;
                            } else {
                                var stepValue = stepValue;
                            }
                            if (typeof attribute !== typeof undefined && attribute !== false){
                                $(this).find('div').text((stepValue).toFixed(0) + " " + $(this).find('div').data("satuan"));
                            } else {
                                $(this).find('div').text((stepValue).toFixed(0));
                            }
                        }).stop();
                    }
                });
            }

            // Show animated elements
            animateElements();
            $(window).scroll(animateElements);

        }); //end document ready function
    </script>
@endsection
