<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="agungtent.com">
    <meta name="publisher" content="agungtent.com">
    <meta name="twitter:site" content="@Shelter_Tents" />
    <meta name="twitter:creator" content="@Shelter_Tents" />

    <!-- Font Awesome Icons -->
    <link title="Agung Tent Asset" alt="Agung Tent Asset" href="{{ asset('site/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Google Fonts -->
    <link title="Agung Tent Asset" alt="Agung Tent Asset" href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
    <link title="Agung Tent Asset" alt="Agung Tent Asset" href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link title="Agung Tent Asset" alt="Agung Tent Asset" href="{{ asset('site/vendor/magnific-popup/magnific-popup.css') }}" rel="stylesheet">

    <!-- Theme CSS - Includes Bootstrap -->
    <link title="Agung Tent Asset" alt="Agung Tent Asset" href="{{ asset('site/css/creative.css') }}" rel="stylesheet">
    <link title="Agung Tent Asset" alt="Agung Tent Asset" href="{{ asset('css/slick.css') }}" rel="stylesheet">
    <link title="Agung Tent Asset" alt="Agung Tent Asset" href="{{ asset('css/slick-theme.css') }}" rel="stylesheet">
    <link title="Agung Tent Asset" alt="Agung Tent Asset" href="{{ asset('css/style.css') }}" rel="stylesheet">
    @yield('meta_section')
</head>

<body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3 navbar-scrolled mainNav-alt" id="mainNav">
        <div class="container" id="app">
            <a class="navbar-brand js-scroll-trigger" href="{{ route('home') }}" title="Agung Tent | Peersewaan dan Jual Beli Tenda" alt="Agung Tent | Peersewaan dan Jual Beli Tenda">
                <img class="img-fluid img-responsive" style="height: 40px;" src="{{$header_logo->image_full}}" title="Agung Tent | Peersewaan dan Jual Beli Tenda" alt="Agung Tent | Peersewaan dan Jual Beli Tenda">
            </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto my-2 my-lg-0">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ route('product_category') }}" alt="Agung Tent | Peersewaan dan Jual Beli Tenda | Produk" title="Agung Tent | Peersewaan dan Jual Beli Tenda | Produk">Produk</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ route('project_list') }}" alt="Agung Tent | Peersewaan dan Jual Beli Tenda | Projek" title="Agung Tent | Peersewaan dan Jual Beli Tenda | Projek">Projek</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ route('about_us') }}" alt="Agung Tent | Peersewaan dan Jual Beli Tenda | Tentang Kami" title="Agung Tent | Peersewaan dan Jual Beli Tenda | Tentang Kami">Tentang Kami</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ route('kontak_kami') }}" alt="Agung Tent | Peersewaan dan Jual Beli Tenda | Kontak" title="Agung Tent | Peersewaan dan Jual Beli Tenda | Kontak">Kontak</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ route('blog_list') }}" alt="Agung Tent | Peersewaan dan Jual Beli Tenda | Blog" title="Agung Tent | Peersewaan dan Jual Beli Tenda | Blog">Blog</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- Call to Action Section -->
    <section class="bg-dark text-white mobile-only">
        <div class="col-lg-12 text-center">
            <div class="row text-center">
                <a href="{{ route('home') }}" title="Agung Tent | Peersewaan dan Jual Beli Tenda Home" alt="Agung Tent | Peersewaan dan Jual Beli Tenda Home" class="col-20 text-center text-white-50">
                    <i class="fa fa-home text-center"></i>
                    <p class="text-center mb-0">Home</p>
                </a>
                <a href="{{ route('product_category') }}" title="Agung Tent | Peersewaan dan Jual Beli Tenda Produk" alt="Agung Tent | Peersewaan dan Jual Beli Tenda Produk" class="col-20 text-center active text-white">
                    <i class="fa fa-campground  text-center"></i>
                    <p class="text-center mb-0">Produk</p>
                </a>
                <a href="{{ route('project_list') }}" title="Agung Tent | Peersewaan dan Jual Beli Tenda Projek" alt="Agung Tent | Peersewaan dan Jual Beli Tenda Projek" class="col-20 text-center text-white-50">
                    <i class="fa fa-igloo text-center"></i>
                    <p class="text-center mb-0">Projek</p>
                </a>
                <a href="{{ route('about_us') }}" title="Agung Tent | Peersewaan dan Jual Beli Tenda Tentang Kami" alt="Agung Tent | Peersewaan dan Jual Beli Tenda Tentang Kami" class="col-20 text-center text-white-50">
                    <i class="fa fa-user text-center"></i>
                    <p class="text-center mb-0">Tentang Kami</p>
                </a>
                <a href="{{ route('kontak_kami') }}" title="Agung Tent | Peersewaan dan Jual Beli Tenda Kontak Kami" alt="Agung Tent | Peersewaan dan Jual Beli Tenda Kontak Kami" class="col-20 text-center text-white-50">
                    <i class="fa fa-phone text-center"></i>
                    <p class="text-center mb-0">Kontak Kami</p>
                </a>
            </div>
        </div>
    </section>   

    <nav class="scroll-up hide-button">
        <i class="fa fa-chevron-up"></i>
    </nav>

    <!-- Call to Action Section -->
    <section class="page-section-hide page-section-footer bg-dark text-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 text-justify">
                    <div class="mt-1">
                        <h3 class="small h4 mb-2">Tentang Kami</h3>
                        {!! $homeAboutUs->content !!}
                        <br>
                        <a class="btn btn-primary btn-sm col-lg-12" href="{{ route('about_us') }}" tabindex="0" title="Agung Tent | Peersewaan dan Jual Beli Tenda Tentang Kami" alt="Agung Tent | Peersewaan dan Jual Beli Tenda Tentang Kami">Readmore</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 text-justify">
                    <div class="mt-1">
                        <h3 class="small h4 mb-2">Dapatkan Penawaran Gratis</h3>
                        <form>
                            <div class="form-group">
                                <input type="text" class="bg-dark text-white form-control form-small" placeholder="Nama" name="nama" required="true">
                                <input type="email" class="bg-dark text-white form-control form-small" placeholder="email" name="email" required="true">
                                <input type="phone" class="bg-dark text-white form-control form-small" placeholder="Telepon" name="phone" required="true">
                                <textarea name="content" placeholder="inquiry anda" class="bg-dark text-white form-control form-small" required="true"></textarea>
                            </div>
                            <button type="submit" class="btn col-lg-12 btn-sm btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 text-justify">
                    <div class="mt-1">
                        <h3 class="small h4 mb-2">Kontak</h3>
                        {!! $contactFooter->content !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Footer -->
    <footer class="bg-light py-4">
        <div class="container">
            <div class="small text-center text-muted">Copyright &copy; 2019 - <a href="http://agungTent.co.id" title="Agung Tent | Peersewaan dan Jual Beli Tenda" alt="Agung Tent | Peersewaan dan Jual Beli Tenda">AgungTent.co.id</a></div>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script title="Agung Tent Asset" alt="Agung Tent Asset" src="{{ asset('site/vendor/jquery/jquery.min.js') }}"></script>
    <script title="Agung Tent Asset" alt="Agung Tent Asset" src="{{ asset('js/app.js') }}"></script>
    <script title="Agung Tent Asset" alt="Agung Tent Asset" src="{{ asset('js/slick.js') }}"></script>
    <!-- <script title="Agung Tent Asset" alt="Agung Tent Asset" src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script> -->
    <script title="Agung Tent Asset" alt="Agung Tent Asset" src="{{ asset('site/vendor/magnific-popup/jquery.magnific-popup.js') }}"></script>
    <script title="Agung Tent Asset" alt="Agung Tent Asset" src="{{ asset('site/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for this template -->
    <script title="Agung Tent Asset" alt="Agung Tent Asset" src="{{ asset('site/js/creative.js') }}"></script>
    <script title="Agung Tent Asset" alt="Agung Tent Asset">
        $('#for-slick').slick();
        $('#klien-kami').slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 3,
            responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }]
        });

        $(function(){  // $(document).ready shorthand
          $('.page-section').fadeIn("fast");
        });

        $(document).ready(function() {

            $(".scroll-up").on("click", function(){
                $("html, body").animate({ scrollTop: 0 }, "slow");
            });
            
            /* Every time the window is scrolled ... */
            $(window).scroll( function(){
            
                /* Check the location of each desired element */
                $('.page-section-hide').each( function(i){
                    
                    var bottom_of_object = $(this).position().top + $(this).outerHeight();
                    var bottom_of_window = $(window).scrollTop() + $(window).height();
                    
                    /* If the object is completely visible in the window, fade it it */
                    if( bottom_of_window + (bottom_of_object - 300) > bottom_of_object ){
                        
                        $(this).animate({'opacity':'1'},1500);
                            
                    }
                    
                }); 
            
            });
            
        });
    </script>
    @yield('extra_js')

</body>

</html>