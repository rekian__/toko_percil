@extends('site.layouts.base_alt')

@section('meta_section')
    <title>Agung Tent | Segala Tenda Untuk Event Anda | Daftar Tenda Agung Tent</title>
    <meta name="title" content="Agung Tent | Segala Tenda Untuk Event Anda | Daftar Tenda Agung Tent"/>
    <meta name="description" content="Persewaan Tenda Segala Event Lengkap, Terpercaya. Menyewakan Tenda Roder, Tenda Dome Geodesic, Tenda Sarnafil, dan Flooring"/>
    <meta name="keywords" content="Sewa, Tenda, Murah, berkualitas, Jawa, Jakarta, Jabodetabek Daftar Tenda Agung Tent"/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="{{ Request::url() }}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Daftar Tenda Agung Tent" />
    <meta property="og:description" content="Persewaan Tenda Segala Event Lengkap, Terpercaya. Menyewakan Tenda Roder, Tenda Dome Geodesic, Tenda Sarnafil, dan Flooring" />
    <meta property="og:image" content="{{ isset($product->productImages[0]) ? $product->productImages[0]->image_thumbnail : asset('site/img/portfolio/thumbnails/1.jpg') }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:site_name" content="http://agungtent.com" />
    <meta property="article:publisher" content="http://agungtent.com" />
    <meta name="twitter:card" content="{{ isset($product->productImages[0]) ? $product->productImages[0]->image_thumbnail : asset('site/img/portfolio/thumbnails/1.jpg') }}" />
    <meta name="twitter:description" content="Persewaan Tenda Segala Event Lengkap, Terpercaya. Menyewakan Tenda Roder, Tenda Dome Geodesic, Tenda Sarnafil, dan Flooring" />
    <meta name="twitter:title" content="Daftar Tenda Agung Tent" />
@endsection

@section('content')
    <section class="page-section" id="product">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-justify">
                    <h2 class="mt-0 text-center">Produk Kategori</h2>
                    <hr class="divider my-4">
                    <div class="row">
                        @foreach($product_categories as $product_category)
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <a href="{{ route('product_category_detail', $product_category->slug) }}" class="row">
                                    <img src="{{ $product_category->image_thumbnail }}" class="col-lg-12 img-fluid" alt="{{ $product_category->title }}" title="{{ $product_category->title }}">
                                </a>
                                <h3 class="small h4 mb-2 mt-2">{{ $product_category->title }}</h3>
                                <p class="small text-muted mb-4">{{ $product_category->description }} <a href="{{ route('product_category_detail', $product_category->slug) }}" >readmore</a></p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="text-center">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination text-center">
                            {{ $product_categories->links() }}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
@endsection
