@extends('site.layouts.base_alt')

@section('meta_section')
    <title>Segala Tenda Untuk Event Anda | Agung Tent</title>
    <meta name="title" content=""/>
    <meta name="description" content=""/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="article:publisher" content="" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:site" content="@Shelter_Tents" />
    <meta name="twitter:creator" content="@Shelter_Tents" />
@endsection

@section('content')
    <section class="page-section page-section-2" id="product">
        <div class="container">
            <div class="row justify-content-center mb-4">
                <div class="col-lg-12 text-justify magnific-this">
                    <h2 class="mt-0 text-center">{{ $post->title }}</h2>
                    <hr class="divider my-4">
                    <div class="row">
                        <div class="col-lg-8 text-muted mb-4 remove-attr">
                            <div class="row mb-4 ">
                                <img class="col-lg-12 img-fluid" src="{{ $post->image_full }}">
                            </div>
                            {!! $post->content !!}
                        </div>
                        <div class="col-lg-4">
                            <h3 class="h4 mb-4 mt-2">Mungkin Anda Tertarik</h3>
                            <div class="text-muted mb-4 you-may-like">
                                <ul>
                                    @foreach($posts as $post)
                                        <li class="text-left mb-2">
                                            <a href="{{ route('blog_detail', $post->slug) }}"><i class="fa fa-chevron-right"></i> {{ $post->title }}</a> 
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <br>
                    <h2 class="mt-0 text-center mt-4">Artikel Terkait</h2>
                    <hr class="divider my-4">
                    <div class="row">
                        <div class="col-lg-12" id="portfolio">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
                                        <a class="portfolio-box" href="#">
                                            <img class="img-fluid" src="{{ asset('site/img/portfolio/thumbnails/1.jpg') }}" alt="">
                                            <div class="portfolio-box-caption p-3">
                                                <div class="project-category text-white-50">
                                                    Kategori Artikel
                                                </div>
                                                <div class="project-name">
                                                    Judul Artikel
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
                                        <a class="portfolio-box" href="#">
                                            <img class="img-fluid" src="{{ asset('site/img/portfolio/thumbnails/2.jpg') }}" alt="">
                                            <div class="portfolio-box-caption p-3">
                                                <div class="project-category text-white-50">
                                                    Kategori Artikel
                                                </div>
                                                <div class="project-name">
                                                    Judul Artikel
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
                                        <a class="portfolio-box" href="#">
                                            <img class="img-fluid" src="{{ asset('site/img/portfolio/thumbnails/3.jpg') }}" alt="">
                                            <div class="portfolio-box-caption p-3">
                                                <div class="project-category text-white-50">
                                                    Kategori Artikel
                                                </div>
                                                <div class="project-name">
                                                    Judul Artikel
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("extra_js")
    <script type="text/javascript">
        $(document).ready(function(){
            var element_img = $(".remove-attr").find("img");
            $(element_img).attr("width","100%");
            $(element_img).attr("height","auto");
        });
    </script>

@endsection
