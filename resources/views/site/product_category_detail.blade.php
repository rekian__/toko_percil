@extends('site.layouts.base_alt')

@section('meta_section')
    <title>Agung Tent | Segala Tenda Untuk Event Anda | {{ $product_category->title }}</title>
    <meta name="title" content="{{ $product_category->title }}"/>
    <meta name="description" content="{{ $product_category->description }}"/>
    <meta name="keywords" content="Sewa, Tenda, Murah, berkualitas, Jawa, Jakarta, Jabodetabek {{ $product_category->title }}"/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="{{ Request::url() }}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ $product_category->title }}" />
    <meta property="og:description" content="{{ $product_category->description }}" />
    <meta property="og:image" content="{{ isset($related_product->productImages[0]) ? $related_product->productImages[0]->image_thumbnail : asset('site/img/portfolio/thumbnails/1.jpg') }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:site_name" content="http://agungtent.com" />
    <meta property="article:publisher" content="http://agungtent.com" />
    <meta name="twitter:card" content="{{ isset($related_product->productImages[0]) ? $related_product->productImages[0]->image_thumbnail : asset('site/img/portfolio/thumbnails/1.jpg') }}" />
    <meta name="twitter:description" content="{{ $product_category->description }}" />
    <meta name="twitter:title" content="{{ $product_category->title }}" />
@endsection

@section('content')
    <section class="page-section page-section-2" id="product">
        <div class="container">
            <div class="row justify-content-center mb-4">
                <div class="col-lg-12 text-justify">
                    <h2 class="mt-0 text-center">Daftar Produk Tenda Pernikahan</h2>
                    <hr class="divider my-4">
                    <div class="row">
                        <div class="col-lg-12" id="portfolio">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    @foreach($product_category->products as $product)
                                        <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
                                            <a class="portfolio-box" href="{{ route('product_detail', $product->slug) }}" alt="{{ $product->title }}" title="{{ $product->title }}">
                                                <div class="row">
                                                    <img class="col-lg-12 img-fluid" src="{{ isset($product->productImages[0]) ? $product->productImages[0]->image_thumbnail : asset('site/img/portfolio/thumbnails/1.jpg') }}" alt="{{ $product->title }}" title="{{ $product->title }}">
                                                </div>
                                                <div class="portfolio-box-caption p-3">
                                                    <div class="project-category text-white-50">
                                                        {{ $product->title }}
                                                    </div>
                                                    <div class="project-name btn-blue-cover mt-2">
                                                        <span class="btn btn-blue text-white">
                                                            Gallery and Spesification
                                                        </span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                    <h2 class="mt-0 text-center mt-4">Tentang {{ $product_category->title }}</h2>
                    <hr class="divider my-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="h4 mb-4 mt-2">{{ $product_category->description }}</h3>
                            <div class="text-muted mb-4">
                                {!! $product_category->content !!}
                            </div>
                        </div>
                    </div>

                    <h2 class="mt-0 text-center mt-4">Inquiry Form</h2>
                    <hr class="divider my-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <form>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nama" name="nama" required="true">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="email" name="email" required="true">
                                </div>
                                <div class="form-group">
                                    <input type="phone" class="form-control" placeholder="Telepon" name="phone" required="true">
                                </div>
                                <div class="form-group">
                                    <textarea name="content" placeholder="inquiry anda" class="form-control" required="true"></textarea>
                                </div>
                                <button type="submit" class="btn col-lg-12 btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                    <br>
                    <h2 class="mt-0 text-center mt-4">Produk Sehubungan</h2>
                    <hr class="divider my-4">
                    <div class="row">
                        <div class="col-lg-12" id="portfolio">
                            <div class="container-fluid p-0">
                                <div class="row ">
                                    @foreach($related_product_categories as $related_product_category)
                                        <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
                                            <a class="portfolio-box" href="{{ route('product_category_detail', $related_product_category->slug) }}" alt="{{ $related_product_category->title }}" title="{{ $related_product_category->title }}">
                                                <div class="row">
                                                    <img class="col-lg-12 img-fluid" src="{{ $related_product_category->image_thumbnail }}" alt="{{ $related_product_category->title }}" title="{{ $related_product_category->title }}">
                                                </div>
                                                <div class="portfolio-box-caption p-3">
                                                    <div class="project-name">
                                                        {{ $related_product_category->title }}
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
