@extends('site.layouts.base_alt')

@section('meta_section')
    <title>Agung Tent | Segala Tenda Untuk Event Anda | {{ $product->title }}</title>
    <meta name="title" content="{{ $product->title }}"/>
    <meta name="description" content="{{ $product->description }}"/>
    <meta name="keywords" content="Sewa, Tenda, Murah, berkualitas, Jawa, Jakarta, Jabodetabek {{ $product->title }}"/>
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="{{ Request::url() }}" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ $product->title }}" />
    <meta property="og:description" content="{{ $product->description }}" />
    <meta property="og:image" content="{{ isset($product->productImages[0]) ? $product->productImages[0]->image_thumbnail : asset('site/img/portfolio/thumbnails/1.jpg') }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:site_name" content="http://agungtent.com" />
    <meta property="article:publisher" content="http://agungtent.com" />
    <meta name="twitter:card" content="{{ isset($product->productImages[0]) ? $product->productImages[0]->image_thumbnail : asset('site/img/portfolio/thumbnails/1.jpg') }}" />
    <meta name="twitter:description" content="{{ $product->description }}" />
    <meta name="twitter:title" content="{{ $product->title }}" />
@endsection

@section('content')
    <section class="page-section page-section-2" id="product">
        <div class="container">
            <div class="row justify-content-center mb-4">
                <div class="col-lg-12 text-justify magnific-this">
                    <h2 class="mt-0 text-center">{{ $product->title }}</h2>
                    <hr class="divider my-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="h4 mb-4 mt-2">{{ $product->description }}</h3>
                            <div class="text-muted mb-4">
                                {!! $product->content !!}
                            </div>
                        </div>
                    </div>

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a title="Agung Tent Gallery dan Spesifikasi" class="nav-link active" id="gallery-tab" data-toggle="tab" href="#gallery" role="tab" aria-controls="gallery" aria-selected="true">Gallery</a>
                        </li>
                        <li class="nav-item">
                            <a title="Agung Tent Gallery dan Spesifikasi" class="nav-link" id="spesifikasi-tab" data-toggle="tab" href="#spesifikasi" role="tab" aria-controls="spesifikasi" aria-selected="false">Spesifikasi</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="gallery" role="tabpanel" aria-labelledby="gallery-tab">
                        <div id="portfolio">
                                <div class="container-fluid p-0">
                                    <div class="row no-gutters">
                                        @foreach($product->productImages as $productImage)
                                            <div class="col-lg-4 col-sm-6">
                                                <a title="{{ $product->title }}" alt="{{ $product->title }}" class="portfolio-box" href="{{ $productImage->image_full }}">
                                                    <div class="row">
                                                        <img title="{{ $product->title }}" alt="{{ $product->title }}" class="col-lg-12 img-fluid" src="{{ $productImage->image_thumbnail }}" alt="">
                                                    </div>
                                                    <div class="portfolio-box-caption">
                                                        <div class="project-category text-white-50">
                                                            <i class="fa fa-search-plus big text-white"></i>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="spesifikasi" role="tabpanel" aria-labelledby="spesifikasi-tab">
                            <div class="text-muted mb-4 mt-4 col-lg-12">
                                {!! $product->spesification !!}
                            </div>
                        </div>
                    </div>

                    <h2 class="mt-0 text-center mt-4">Inquiry Form</h2>
                    <hr class="divider my-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <form>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nama" name="nama" required="true">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="email" name="email" required="true">
                                </div>
                                <div class="form-group">
                                    <input type="phone" class="form-control" placeholder="Telepon" name="phone" required="true">
                                </div>
                                <div class="form-group">
                                    <textarea name="content" placeholder="inquiry anda" class="form-control" required="true"></textarea>
                                </div>
                                <button type="submit" class="btn col-lg-12 btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                    <br>
                    <h2 class="mt-0 text-center mt-4">Produk Sehubungan</h2>
                    <hr class="divider my-4">
                    <div class="row">
                        <div class="col-lg-12" id="portfolio">
                            <div class="container-fluid p-0">
                                <div class="row">
                                    @foreach($related_products as $related_product)
                                        <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
                                            <a class="portfolio-box" href="{{ route('product_detail', $related_product->slug) }}" title="{{$related_product->title}}" alt="{{$related_product->title}}">
                                                <div class="row">
                                                    <img class="col-lg-12 img-fluid" src="{{ isset($related_product->productImages[0]) ? $related_product->productImages[0]->image_thumbnail : asset('site/img/portfolio/thumbnails/1.jpg') }}" title="{{$related_product->title}}" alt="{{$related_product->title}}">
                                                </div>
                                                <div class="portfolio-box-caption p-3">
                                                    <div class="project-category text-white-50">
                                                        {{$related_product->productCategory->title}}
                                                    </div>
                                                    <div class="project-name">
                                                        {{$related_product->title}}
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
