<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 3 | Projects</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- overlayScrollbars -->
    <link href="{{ asset('site/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adminlte-1.css') }}" rel="stylesheet">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper" id="app">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Messages Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <form action="{{ route('logout') }}" method="post">
                            <input method="post" class="btn btn-primary dropdown-item dropdown-footer" type="submit" value="Logout" />
                            {!! csrf_field() !!}
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{ action('Admin\DashboardController@index') }}" class="brand-link">
                <img src="/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">Agung Tent</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user (optional) -->
                <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">Alexander Pierce</a>
                    </div>
                </div> -->

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item ">
                            <a href="{{ action('Admin\SliderController@index') }}" class="nav-link {{ strpos(Request::route()->getName(), 'sliders') !== false ? 'active' : '' }}">
                                <i class="nav-icon far {{ strpos(Request::route()->getName(), 'sliders') === false ? 'fa-circle' : 'fa-dot-circle' }}"></i>
                                <p>
                                    Slider
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ action('Admin\ProjectCategoryController@index') }}" class="nav-link {{ strpos(Request::route()->getName(), 'project-categories') !== false ? 'active' : '' }}">
                                <i class="nav-icon far {{ strpos(Request::route()->getName(), 'project-categories') === false ? 'fa-circle' : 'fa-dot-circle' }}"></i>
                                <p>
                                    Project Category
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ action('Admin\ProjectController@index') }}" class="nav-link {{ strpos(Request::route()->getName(), 'projects') !== false ? 'active' : '' }}">
                                <i class="nav-icon far {{ strpos(Request::route()->getName(), 'projects') === false ? 'fa-circle' : 'fa-dot-circle' }}"></i>
                                <p>
                                    Project
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ action('Admin\ProductCategoryController@index') }}" class="nav-link {{ strpos(Request::route()->getName(), 'product-categories') !== false ? 'active' : '' }}">
                                <i class="nav-icon far {{ strpos(Request::route()->getName(), 'product-categories') === false ? 'fa-circle' : 'fa-dot-circle' }}"></i>
                                <p>
                                    Product Category
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ action('Admin\ProductController@index') }}" class="nav-link {{ strpos(Request::route()->getName(), 'products') !== false ? 'active' : '' }}">
                                <i class="nav-icon far {{ strpos(Request::route()->getName(), 'products') === false ? 'fa-circle' : 'fa-dot-circle' }}"></i>
                                <p>
                                    Product
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ action('Admin\PostController@index') }}" class="nav-link {{ strpos(Request::route()->getName(), 'posts') !== false ? 'active' : '' }}">
                                <i class="nav-icon far {{ strpos(Request::route()->getName(), 'posts') === false ? 'fa-circle' : 'fa-dot-circle' }}"></i>
                                <p>
                                    Post
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ action('Admin\PageTypeController@index') }}" class="nav-link {{ strpos(Request::route()->getName(), 'PageTypes') !== false ? 'active' : '' }}">
                                <i class="nav-icon far {{ strpos(Request::route()->getName(), 'PageTypes') === false ? 'fa-circle' : 'fa-dot-circle' }}"></i>
                                <p>
                                    PageType
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ action('Admin\PageController@index') }}" class="nav-link {{ strpos(Request::route()->getName(), 'Pages') !== false ? 'active' : '' }}">
                                <i class="nav-icon far {{ strpos(Request::route()->getName(), 'Pages') === false ? 'fa-circle' : 'fa-dot-circle' }}"></i>
                                <p>
                                    Page
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ action('Admin\ContactUsController@index') }}" class="nav-link {{ strpos(Request::route()->getName(), 'contact-us') !== false ? 'active' : '' }}">
                                <i class="nav-icon far {{ strpos(Request::route()->getName(), 'contact-us') === false ? 'fa-circle' : 'fa-dot-circle' }}"></i>
                                <p>
                                    Contact Us
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            @yield("title")
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 text-right">
                            @yield("create_button")
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </section>

            <!-- Main content -->
       		@yield("content")
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 3.0.1
            </div>
            <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
        </footer>
        @yield("modal")
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('js/adminlte-1.js') }}"></script>
    <script src="{{ asset('js/tinymce.min.js') }}"></script>
    <script>
        if ($(".text_area").length != undefined) {
            tinymce.init({
                selector: '.text_area',
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste imagetools wordcount"
                  ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                images_upload_handler: function (blobInfo, success, failure) {
                   var xhr, formData;
                   xhr = new XMLHttpRequest();
                   xhr.withCredentials = false;
                   xhr.open('POST', '/admin/upload/image');
                   var token = '{{ csrf_token() }}';
                   xhr.setRequestHeader("X-CSRF-Token", token);
                   xhr.onload = function() {
                       var json;
                       if (xhr.status != 200) {
                           failure('HTTP Error: ' + xhr.status);
                           return;
                       }
                       json = JSON.parse(xhr.responseText);

                       if (!json || typeof json.location != 'string') {
                           failure('Invalid JSON: ' + xhr.responseText);
                           return;
                       }
                       success(json.location);
                   };
                   formData = new FormData();
                   formData.append('file', blobInfo.blob(), blobInfo.filename());
                   xhr.send(formData);
                }
            });
        }
    </script>
    @yield("extra_js")
</body>

</html>