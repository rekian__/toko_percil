@extends('admin.layouts.base')

@section("title")
    <h1>Sliders - Form</h1>
@endsection
@section("content")
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Sliders</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" enctype="multipart/form-data" method="POST" action="{{ isset($slider) ? action('Admin\SliderController@update', ['id'=>$slider->id]) : action('Admin\SliderController@store') }}">
                            @if(isset($slider))
                                @method('PUT')
                            @endif
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="url">Url</label>
                                    <input type="text" class="form-control" value="{{old('url', isset($slider) ? $slider->url : '')}}" id="url" name="url" placeholder="Enter Url">
                                    @if ($errors->has('url'))
                                        <div class="error text-info">{{ $errors->first('url') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="image">Image Slider</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="image" name="image">
                                            <label class="custom-file-label" for="image">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                    @if ($errors->has('image'))
                                        <div class="error text-info">{{ $errors->first('image') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="caption">Caption</label>
                                    <input type="text" class="form-control" value="{{old('caption', isset($slider) ? $slider->caption : '')}}" id="caption" name="caption" placeholder="Enter Caption">
                                    @if ($errors->has('caption'))
                                        <div class="error text-info">{{ $errors->first('caption') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea type="text" class="form-control" value="" id="description" name="description" placeholder="Enter Description">{{old('description', isset($slider) ? $slider->description : '')}}</textarea>
                                    @if ($errors->has('description'))
                                        <div class="error text-info">{{ $errors->first('description') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="text_style">Text Style</label>
                                    <select class="form-control" id="text_style" name="text_style">
                                        @for($i = 1; $i <= 3; $i++)
                                            <option value="{{ $i }}" {{ old('text_style') == $i ? 'selected' : isset($slider) && $slider->text_style == $i && old('text_style') == null ? 'selected' : '' }} >Style {{$i}}</option>
                                        @endfor
                                    </select>
                                    @if ($errors->has('text_style'))
                                        <div class="error text-info">{{ $errors->first('text_style') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="position">Position</label>
                                    <select class="form-control" id="position" name="position">
                                        @foreach($positions as $key => $position)
                                            <option value="{{ $key }}" {{ old('position') == $key ? 'selected' : isset($slider) && $slider->position == $key && old('position') == null ? 'selected' : '' }} >{{$position}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('position'))
                                        <div class="error text-info">{{ $errors->first('position') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status">
                                        @for($i = 1; $i <= 2; $i++)
                                            <option value="{{ $i }}" {{ old('status') == $i ? 'selected' : isset($slider) && $slider->status == $i && old('status') == null ? 'selected' : '' }} >{{ $i == 1 ? "Published" : "Draft" }}</option>
                                        @endfor
                                    </select>
                                    @if ($errors->has('status'))
                                        <div class="error text-info">{{ $errors->first('status') }}</div>
                                    @endif
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection