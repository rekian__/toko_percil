@extends('admin.layouts.base')

@section("title")
    <h1>Sliders - {{ $slider->caption }}</h1>
@endsection
@section("content")
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Sliders</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="url">Url</label>
                                <h4>{{ $slider->url }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="image">Image Slider</label>
                                <div class="row col-lg-6">
                                    <img class="img-fluid" src="{{ $slider->image }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="caption">Caption</label>
                                <h4>{{ $slider->caption }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <h4>{{ $slider->description }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <h4>{{ $slider->status }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection