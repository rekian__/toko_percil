@extends('admin.layouts.base')

@section("title")
    <h1>Page - Form</h1>
@endsection
@section("content")
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Page</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" enctype="multipart/form-data" method="POST" action="{{ isset($page) ? action('Admin\PageController@update', ['id'=>$page->id]) : action('Admin\PageController@store') }}">
                            @if(isset($page))
                                @method('PUT')
                            @endif
                            @csrf
                            <div class="card-body">
                                @component("admin.components.page_form", ["page_types" => $page_types, "page" => $page, "listed_forms" => $listed_forms])
                                @endcomponent
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection