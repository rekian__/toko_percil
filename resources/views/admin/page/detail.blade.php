@extends('admin.layouts.base')

@section("title")
    <h1>Pages - {{ $page->title }}</h1>
@endsection
@section("content")
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Pages</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <h4>{{ $page->title }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                <h4>{{ $page->slug }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <h4>{{ $page->description }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <h4>{!! $page->content !!}</h4>
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <h4>{{ $page->status }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="status">images</label>
                                <div class="row">
                                    @foreach($page->pageImages as $pageImage)
                                        <div class="col-lg-4">
                                            <img class="img-fluid" src="{{ $pageImage->image_thumbnail }}">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection