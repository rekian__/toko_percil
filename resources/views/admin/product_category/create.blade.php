@extends('admin.layouts.base')

@section("title")
    <h1>Product Categories - Form</h1>
@endsection
@section("content")
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Product Categories</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" enctype="multipart/form-data" method="POST" action="{{ isset($product_category) ? action('Admin\ProductCategoryController@update', ['id'=>$product_category->id]) : action('Admin\ProductCategoryController@store') }}">
                            @if(isset($product_category))
                                @method('PUT')
                            @endif
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" value="{{old('title', isset($product_category) ? $product_category->title : '')}}" id="title" name="title" placeholder="Enter Title">
                                    @if ($errors->has('title'))
                                        <div class="error text-info">{{ $errors->first('title') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea type="text" class="form-control" value="" id="description" name="description" placeholder="Enter Description">{{old('description', isset($product_category) ? $product_category->description : '')}}</textarea>
                                    @if ($errors->has('description'))
                                        <div class="error text-info">{{ $errors->first('description') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="content">Content</label>
                                    <textarea type="text" class="form-control text_area"  id="content" name="content" placeholder="Enter Content">
                                        {{ old('content', isset($product_category) ? $product_category->content : '') }}
                                    </textarea> 
                                    @if ($errors->has('content'))
                                        <div class="error text-info">{{ $errors->first('content') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="image">Image Product Category</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="image" name="image">
                                            <label class="custom-file-label" for="image">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                    @if ($errors->has('image'))
                                        <div class="error text-info">{{ $errors->first('image') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="caption">Caption</label>
                                    <input type="text" class="form-control" value="{{old('caption', isset($product_category) ? $product_category->caption : '')}}" id="caption" name="caption" placeholder="Enter Caption">
                                    @if ($errors->has('caption'))
                                        <div class="error text-info">{{ $errors->first('caption') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status">
                                        @for($i = 1; $i <= 2; $i++)
                                            <option value="{{ $i }}" {{ old('status') == $i ? 'selected' : isset($product_category) && $product_category->status == $i && old('status') == null ? 'selected' : '' }} >{{ $i == 1 ? "Published" : "Draft" }}</option>
                                        @endfor
                                    </select>
                                    @if ($errors->has('status'))
                                        <div class="error text-info">{{ $errors->first('status') }}</div>
                                    @endif
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection