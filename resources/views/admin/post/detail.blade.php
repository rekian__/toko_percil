@extends('admin.layouts.base')

@section("title")
    <h1>Posts - {{ $post->title }}</h1>
@endsection
@section("content")
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Posts</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="url">Title</label>
                                <h4>{{ $post->title }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="url">Slug</label>
                                <h4>{{ $post->slug }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="image">Image</label>
                                <div class="row col-lg-6">
                                    <img class="img-fluid" src="{{ $post->image_full }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="caption">Caption</label>
                                <h4>{{ $post->caption }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <h4>{{ $post->description }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="description">Content</label>
                                <h4>{!! $post->content !!}</h4>
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <h4>{{ $post->status }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection