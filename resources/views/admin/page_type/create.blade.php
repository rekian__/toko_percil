@extends('admin.layouts.base')

@section("title")
    <h1>Page Types - Form</h1>
@endsection
@section("content")
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Page Types</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" enctype="multipart/form-data" method="POST" action="{{ isset($page_type) ? action('Admin\PageTypeController@update', ['id'=>$page_type->id]) : action('Admin\PageTypeController@store') }}">
                            @if(isset($page_type))
                                @method('PUT')
                            @endif
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" value="{{old('title', isset($page_type) ? $page_type->title : '')}}" id="title" name="title" placeholder="Enter Title">
                                    @if ($errors->has('title'))
                                        <div class="error text-info">{{ $errors->first('title') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="form_element">Form Element</label>
                                    <textarea type="text" class="form-control" value="" id="form_element" name="form_element" placeholder="Enter Form Element">{{old('form_element', isset($product) ? $product->form_element : '')}}</textarea>
                                    @if ($errors->has('form_element'))
                                        <div class="error text-info">{{ $errors->first('form_element') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status">
                                        @for($i = 1; $i <= 2; $i++)
                                            <option value="{{ $i }}" {{ old('status') == $i ? 'selected' : isset($page_type) && $page_type->status == $i && old('status') == null ? 'selected' : '' }} >{{ $i == 1 ? "Published" : "Draft" }}</option>
                                        @endfor
                                    </select>
                                    @if ($errors->has('status'))
                                        <div class="error text-info">{{ $errors->first('status') }}</div>
                                    @endif
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection