@if( in_array("title", $listed_forms))
	<div class="form-group">
	    <label for="title">Title</label>
	    <input type="text" class="form-control" value="{{old('title', isset($page) ? $page->title : '')}}" id="title" name="title" placeholder="Enter Title">
	    @if ($errors->has('title'))
	        <div class="error text-info">{{ $errors->first('title') }}</div>
	    @endif
	</div>
@endif
@if( in_array("description", $listed_forms))
	<div class="form-group">
	    <label for="description">Description</label>
	    <textarea type="text" class="form-control" value="" id="description" name="description" placeholder="Enter Description">{{old('description', isset($page) ? $page->description : '')}}</textarea>
	    @if ($errors->has('description'))
	        <div class="error text-info">{{ $errors->first('description') }}</div>
	    @endif
	</div>
@endif
@if( in_array("content", $listed_forms))
	<div class="form-group">
	    <label for="content">Content</label>
	    <textarea type="text" class="form-control text_area"  id="content" name="content" placeholder="Enter Content">
	        {{ old('content', isset($page) ? $page->content : '') }}
	    </textarea> 
	    @if ($errors->has('content'))
	        <div class="error text-info">{{ $errors->first('content') }}</div>
	    @endif
	</div>
@endif
@if( in_array("page_type_id", $listed_forms))
	<div class="form-group">
	    <label>Page Category</label>
	    <select class="form-control" id="page_type_id" name="page_type_id">
	        @foreach($page_types as $page_type)
	        <option value="{{ $page_type->id }}" {{ old('page_type_id') == $page_type->id ? 'selected' : isset($page) && $page->pageType->id == $page_type->id && old('page_type_id') == null ? 'selected' : '' }} > {{ $page_type->title }} </option>
	        @endforeach
	    </select>
	    @if ($errors->has('page_type_id'))
	        <div class="error text-info">{{ $errors->first('page_type_id') }}</div>
	    @endif
	</div>
@endif
@if( in_array("status", $listed_forms))
	<div class="form-group">
	    <label for="status">Status</label>
	    <select class="form-control" id="status" name="status">
	        @for($i = 1; $i <= 2; $i++)
	            <option value="{{ $i }}" {{ old('status') == $i ? 'selected' : isset($page) && $page->status == $i && old('status') == null ? 'selected' : '' }} >{{ $i == 1 ? "Published" : "Draft" }}</option>
	        @endfor
	    </select>
	    @if ($errors->has('status'))
	        <div class="error text-info">{{ $errors->first('status') }}</div>
	    @endif
	</div>
@endif
@if( in_array("image", $listed_forms))
	<div class="form-group">
	    <label for="image">Image</label>
	    <div class="input-group">
	        <div class="custom-file">
	            <input type="file" class="custom-file-input" id="image" name="image">
	            <label class="custom-file-label" for="image">Choose file</label>
	        </div>
	        <div class="input-group-append">
	            <span class="input-group-text" id="">Upload</span>
	        </div>
	    </div>
	    <div class="clearfix"></div><br><br>
	    <div class="row">
		    <div class="col-lg-12">
		    	<img class="img-fluid" src="{{ isset( $page ) ?  $page->image_full : '' }}">
		    </div>
	    </div>
	    @if ($errors->has('image'))
	        <div class="error text-info">{{ $errors->first('image') }}</div>
	    @endif
	</div>
@endif
@if( in_array("caption", $listed_forms))
	<div class="form-group">
	    <label for="caption">{{ $page->pageType->slug == 'about-us-team' ? 'Linkedin URL' : 'Caption' }}</label>
	    <input type="text" class="form-control" value="{{old('caption', isset($page) ? $page->caption : '')}}" id="caption" name="caption" placeholder="Enter {{ $page->pageType->slug == 'about-us-team' ? 'Linkedin URL' : 'Caption' }}">
	    @if ($errors->has('caption'))
	        <div class="error text-info">{{ $errors->first('caption') }}</div>
	    @endif
	</div>
@endif
@if( in_array("images[]", $listed_forms))
	<div class="col-lg-12 card-header">
	    <div class="form-group">
	        <label for="images[]">Image Page</label>
	        <div class="input-group">
	            <div class="custom-file">
	                <input type="file" class="custom-file-input" id="images[]" name="images[]" multiple>
	                <label class="custom-file-label" for="images[]">Choose file</label>
	            </div>
	            <div class="input-group-append">
	                <span class="input-group-text" id="">Upload</span>
	            </div>
	        </div>
	        @if ($errors->has('images[]'))
	            <div class="error text-info">{{ $errors->first('images[]') }}</div>
	        @endif
	    </div>
	    @if(isset($page->pageImages))
		    <div class="clearfix"></div><br>
		    <div class="row">
			    @foreach($page->pageImages as $pageImage)
				    <div class="col-lg-2">
				    	<img class="img-fluid" src="{{ isset( $pageImage ) ?  $pageImage->image_full : '' }}">
				    </div>
			    @endforeach
		    </div>
	    @endif
	    <br>
	</div>
@endif