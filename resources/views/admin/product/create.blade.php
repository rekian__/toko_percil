@extends('admin.layouts.base')

@section("title")
    <h1>Product - Form</h1>
@endsection
@section("content")
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Product</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" enctype="multipart/form-data" method="POST" action="{{ isset($product) ? action('Admin\ProductController@update', ['id'=>$product->id]) : action('Admin\ProductController@store') }}">
                            @if(isset($product))
                                @method('PUT')
                            @endif
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" value="{{old('title', isset($product) ? $product->title : '')}}" id="title" name="title" placeholder="Enter Title">
                                    @if ($errors->has('title'))
                                        <div class="error text-info">{{ $errors->first('title') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea type="text" class="form-control" value="" id="description" name="description" placeholder="Enter Description">{{old('description', isset($product) ? $product->description : '')}}</textarea>
                                    @if ($errors->has('description'))
                                        <div class="error text-info">{{ $errors->first('description') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="content">Content</label>
                                    <textarea type="text" class="form-control text_area"  id="content" name="content" placeholder="Enter Content">
                                        {{ old('content', isset($product) ? $product->content : '') }}
                                    </textarea> 
                                    @if ($errors->has('content'))
                                        <div class="error text-info">{{ $errors->first('content') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="spesification">Spesification</label>
                                    <textarea type="text" class="form-control text_area"  id="spesification" name="spesification" placeholder="Enter Spesification">
                                        {{ old('spesification', isset($product) ? $product->spesification : '') }}
                                    </textarea> 
                                    @if ($errors->has('spesification'))
                                        <div class="error text-info">{{ $errors->first('spesification') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Product Category</label>
                                    <select class="form-control" id="product_category_id" name="product_category_id">
                                        @foreach($product_categories as $product_category)
                                        <option value="{{ $product_category->id }}" {{ old('product_category_id') == $product_category->id ? 'selected' : isset($product) && $product->productCategory->id == $product_category->id && old('product_category_id') == null ? 'selected' : '' }} >{{ $product_category->title }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('product_category_id'))
                                        <div class="error text-info">{{ $errors->first('product_category_id') }}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status">
                                        @for($i = 1; $i <= 2; $i++)
                                            <option value="{{ $i }}" {{ old('status') == $i ? 'selected' : isset($product) && $product->status == $i && old('status') == null ? 'selected' : '' }} >{{ $i == 1 ? "Published" : "Draft" }}</option>
                                        @endfor
                                    </select>
                                    @if ($errors->has('status'))
                                        <div class="error text-info">{{ $errors->first('status') }}</div>
                                    @endif
                                </div>
                                <div class="col-lg-12 card-header">
                                    <div class="form-group">
                                        <label for="images[]">Image Product</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="images[]" name="images[]" multiple>
                                                <label class="custom-file-label" for="images[]">Choose file</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="">Upload</span>
                                            </div>
                                        </div>
                                        @if ($errors->has('images[]'))
                                            <div class="error text-info">{{ $errors->first('images[]') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection