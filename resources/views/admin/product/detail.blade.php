@extends('admin.layouts.base')

@section("title")
    <h1>Products - {{ $product->title }}</h1>
@endsection
@section("content")
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Products</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <h4>{{ $product->title }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                <h4>{{ $product->slug }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <h4>{{ $product->description }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <h4>{!! $product->content !!}</h4>
                            </div>
                            <div class="form-group">
                                <label for="spesification">Spesification</label>
                                <h4>{!! $product->spesification !!}</h4>
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <h4>{{ $product->status }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="status">images</label>
                                <div class="row">
                                    @foreach($product->productImages as $productImage)
                                        <div class="col-lg-4">
                                            <img class="img-fluid" src="{{ $productImage->image_thumbnail }}">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection