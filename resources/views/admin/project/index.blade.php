@extends('admin.layouts.base')

@section("title")
    <h1>Project - List</h1>
@endsection
@section("create_button")
    <a href="{{ action('Admin\ProjectController@create') }}" class="btn btn-primary"><i class="fas fa-plus"></i> Add Project</a>
@endsection
@section("content")
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-body p-0">
                <table class="table table-striped projects">
                    <thead>
                        <tr>
                            <th style="width: 1%">
                                #
                            </th>
                            <th style="width: 20%">
                                Title
                            </th>
                            <th style="width: 30%">
                                Slug
                            </th>
                            <th>
                                Description
                            </th>
                            <th style="width: 8%" class="text-center">
                                Status
                            </th>
                            <th style="width: 20%">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($projects) > 0)
                            @foreach($projects as $project)
                                <tr>
                                    <td>
                                        {{ $project->id }}
                                    </td>
                                    <td>
                                        {{ $project->title }}
                                    </td>
                                    <td>
                                        {{ $project->slug }}
                                    </td>
                                    <td>
                                        {{ $project->description }}
                                    </td>
                                    <td class="project-state">
                                        @if( $project->status == 1 )
                                            <span class="badge badge-success">Published</span>
                                        @else
                                            <span class="badge badge-warning">Draft</span>
                                        @endif
                                    </td>
                                    <td class="project-actions text-right">
                                        <a class="btn btn-primary btn-sm" href="{{ action('Admin\ProjectController@show', ['id'=>$project->id]) }}">
                                            <i class="fas fa-folder">
                                          </i> View
                                        </a>
                                        <a class="btn btn-info btn-sm" href="{{ action('Admin\ProjectController@edit', ['id'=>$project->id]) }}">
                                            <i class="fas fa-pencil-alt">
                                          </i> Edit
                                        </a>
                                        <form action="{{ action('Admin\ProjectController@destroy', ['id'=>$project->id]) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button class='btn btn-danger btn-sm' type="submit" name="delete_data" value="delete">
                                                <i class="fas fa-trash"></i> Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" class="text-center">
                                    No Data Recorded
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <nav aria-label="Page navigation success">
            <ul class="pagination justify-content-end">
                {{ $projects->links() }}
            </ul>
        </nav>
        <!-- /.card -->
    </section>
@endsection
@section("modal")
    <div id="confirm" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Warning</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="delete">Delete</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("extra_js")
    <script type="text/javascript">
        $('button[name="delete_data"]').on('click', function(e) {
          var $form = $(this).closest('form');
          e.preventDefault();
          $('#confirm').modal({
              backdrop: 'static',
              keyboard: false
          })
          .on('click', '#delete', function(e) {
              $form.trigger('submit');
            });
        });
    </script>
@endsection