@extends('admin.layouts.base')

@section("title")
    <h1>Contact Us - List</h1>
@endsection
@section("content")
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-body p-0">
                <table class="table table-striped projects">
                    <thead>
                        <tr>
                            <th style="width: 1%">#</th>
                            <th style="width: 20%">Title</th>
                            <th style="width: 30%">Description</th>
                            <th style="width: 8%" class="text-center">Status</th>
                            <th style="width: 20%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($contacts) > 0)
                            @foreach($contacts as $contact)
                            <tr>
                                <td>{{ $contact->id }}</td>
                                <td>{{ $contact->title }}</td>
                                <td>{{ $contact->description }}</td>
                                <td class="project-state">
                                    @if( $contact->status == 1 )
                                        <span class="badge badge-success">Published</span>
                                    @else
                                        <span class="badge badge-warning">Draft</span>
                                    @endif
                                </td>
                                <td class="project-actions text-right">
                                    <a class="btn btn-primary btn-sm" href="{{ action('Admin\ContactUsController@show', ['id'=>$contact->id]) }}">
                                        <i class="fas fa-folder"></i> View
                                    </a>
                                    <form action="{{ action('Admin\ContactUsController@destroy', ['id'=>$contact->id]) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class='btn btn-danger btn-sm' type="submit" name="delete_data" value="delete">
                                            <i class="fas fa-trash"></i> Delete
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">
                                    No Data Recorded
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <nav aria-label="Page navigation success">
            <ul class="pagination justify-content-end">
                {{ $contacts->links() }}
            </ul>
        </nav>
        <!-- /.card -->
    </section>
@endsection
@section("modal")
    <div id="confirm" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Warning</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="delete">Delete</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("extra_js")
    <script type="text/javascript">
        $('button[name="delete_data"]').on('click', function(e) {
          var $form = $(this).closest('form');
          e.preventDefault();
          $('#confirm').modal({
              backdrop: 'static',
              keyboard: false
          })
          .on('click', '#delete', function(e) {
              $form.trigger('submit');
            });
        });
    </script>
@endsection