@extends('admin.layouts.base')

@section("title")
    <h1>Contact Us - {{ $contact->title }}</h1>
@endsection
@section("content")
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Contact Us</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <h4>{{ $contact->title }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <h4>{{ $contact->description }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <h4>{{ $contact->content }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="reply_to">Reply To</label>
                                <h4>{{ $contact->reply_to }}</h4>
                            </div>
                            <div class="form-group">
                                <label for="status">Status</label>
                                <h4>{{ $contact->status }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection