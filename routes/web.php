<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 
	'HomeController@index')->name('home');

Route::get('/kategori-produk', 
	'HomeController@product_category')->name('product_category');

Route::get('/kategori-produk/{slug}', 
	'HomeController@product_category_detail')->name('product_category_detail');

Route::get('/produk/{slug}', 
	'HomeController@product_detail')->name('product_detail');

Route::get('/blog', 
	'HomeController@blog_list')->name('blog_list');

Route::get('/blog/{slug}', 
	'HomeController@blog_detail')->name('blog_detail');

Route::get('/projek', 
	'HomeController@project_list')->name('project_list');

Route::get('/projek/{slug}', 
	'HomeController@project_detail')->name('project_detail');

Route::get('/kategori-projek/{slug}', 
	'HomeController@project_category')->name('project_category');

Route::get('/kontak-kami', 
	'HomeController@kontak_kami')->name('kontak_kami');

Route::get('/tentang-kami', 
	'HomeController@about_us')->name('about_us');

Route::post('/admin/upload/image', 'Admin\DashboardController@upload_image');

Route::resource('/admin/dashboard', 'Admin\DashboardController');
Route::resource('/admin/sliders', 'Admin\SliderController');
Route::resource('/admin/products', 'Admin\ProductController');
Route::resource('/admin/product-categories', 'Admin\ProductCategoryController');
Route::resource('/admin/projects', 'Admin\ProjectController');
Route::resource('/admin/project-categories', 'Admin\ProjectCategoryController');
Route::resource('/admin/contact-us', 'Admin\ContactUsController');
Route::resource('/admin/posts', 'Admin\PostController');
Route::resource('/admin/page-types', 'Admin\PageTypeController');
Route::resource('/admin/pages', 'Admin\PageController');
